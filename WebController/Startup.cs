using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MyInterface.Res;
using Newtonsoft.Json;
using NLog.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using DBModel_EFCore;
using IContainer = Autofac.IContainer;
using Server;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Web
{

    public class Startup
    {
        private FileInfo configue;
        public Startup(IHostingEnvironment env)
        {
            var conf = new ConfigurationBuilder()
              .SetBasePath(env.ContentRootPath)
              .AddJsonFile("appsettings.json", true, true)
              .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
              // .AddJsonFile("appsettings_test.json", true, true)
              .AddEnvironmentVariables();
            Configuration = conf.Build();
            HostingEnvironment = env;
        }
        private readonly List<string> fileexit = new List<string> {
            ".json",".xml",".config"
        };
        public IConfiguration Configuration { get; }
        public static IContainer Container { get; private set; }
        public IHostingEnvironment HostingEnvironment { get; set; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            NLog.LogManager.LoadConfiguration("nlog.config");
            services.AddResponseCompression();

            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                            .RequireAuthenticatedUser()
                            .Build();
                config.Filters.Add(new AuthorizeFilter(policy));//添加授权认证
                                                                //config.Filters.Add(new enablecors)
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
               .AddJsonOptions(option =>
               {
                   //                        Newtonsoft.Json.Serialization.DefaultContractResolver
                   option.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();//驼峰式显示
                                                                                                             //   option.SerializerSettings.ContractResolver = new DefaultContractResolver();
               });
            services.AddSwaggerGen(c =>
            {

                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
                // Set the comments path for the Swagger JSON and UI.
                foreach (var xml in Directory.GetFiles(AppContext.BaseDirectory).Where(f => f.EndsWith(".xml")))
                {
                    c.IncludeXmlComments(xml, true);
                }
            });
            services.AddAuth();
            services.AddLogging(logbuilder =>
            {
                logbuilder.SetMinimumLevel(LogLevel.Trace);
                logbuilder.AddNLog(new NLogProviderOptions
                {
                    CaptureMessageTemplates = true,
                    CaptureMessageProperties = true,
                    IncludeScopes = true,
                    ParseMessageTemplates = true
                });
            });
            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.AddMysqlDBContext(Configuration);
            builder.AddDoMainServers();
            builder.AddOtherServer(Configuration);
            builder.AddMiddlerWare();
            Container = builder.Build();

        //    Container.StartTimingServers();

            return new AutofacServiceProvider(Container);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddEventSourceLogger();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }


            // app.UseHsts();
            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            //app.UseCors();
            app.UseAuthentication();
            app.UseMiddleWare();
            app.UseStatusCodePages(new StatusCodePagesOptions()//html和mvc的code404的转向
            {
                HandleAsync = async code =>
                {
                    ;
                    if (code.HttpContext.Response.StatusCode == 404)
                    {
                        var url = code.HttpContext.Request.Path;
                        // if (!url.Value.StartsWith("/api")) return;
                        string exp = Path.GetExtension(url);
                        if (exp == "" || exp == ".html" || exp == ".htm")
                        {
                            List<string> pstrs = url.Value.Split('/', '\\').Skip(2).ToList();
                            string path = "api/Error/404";
                            if (pstrs.Count > 0)
                            {
                                path = pstrs.Select(s => "..").Aggregate((a, b) => a + "/" + b) + "/" + path;
                            }

                            code.HttpContext.Response.Redirect(path, false);

                            return;
                        }
                    }
                    await code.Next(code.HttpContext);
                }
            });
            app.UseMvcWithDefaultRoute();
            app.UseMvc();
        }
    }
}
