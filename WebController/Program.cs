using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Autofac.Extensions.DependencyInjection;

namespace Web
{
    public class Program
    {
        private static Dictionary<string, string> switchkeys = new Dictionary<string, string>
        {
            ["--asp_env"] = "ASPNETCORE_ENVIRONMENT",
        };
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            int port = -1;
            if (args.Length > 0)
            {
                foreach (var str in args)
                {
                    string[] keyvalue = str.Split("=");
                    switch (keyvalue[0])
                    {
                        case "--port": port = Convert.ToInt32(keyvalue[1]); break;
                    }
                }
            }
            var config = new ConfigurationBuilder()
                           .AddCommandLine(args, switchkeys)
                           .Build();
            var builder = WebHost.CreateDefaultBuilder(args)
                    .UseKestrel(option =>
                    {
                        option.ListenLocalhost(port == -1 ? 5000 : 5010, lisen =>
                        {
                            lisen.Protocols = HttpProtocols.Http1AndHttp2;
                            lisen.UseHttps("key/localhost.pfx", "1442241179");
                        });
                    })
                    .ConfigureServices(services => services.AddAutofac())
                    .UseContentRoot(Directory.GetCurrentDirectory())
                    .UseConfiguration(config)
                   .UseStartup<Startup>();
            if (config.GetValue<string>("ASPNETCORE_ENVIRONMENT") is string env)
            {
                builder.UseEnvironment(env);
            }


            return builder;
        }
    }
}
