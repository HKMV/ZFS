﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AllApplication;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Options;

namespace Web.Controllers
{
    /// <summary>
    ///apiv2 的基础路由
    /// </summary>
    [Route("api/[Controller]")]
    [Produces("application/json")]
    [Authorize]
    public class BaseController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        protected IServerContext ServerContext
        {
            get
            {
                return HttpContext.User as IServerContext;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public BaseController()
        {

        }
    }
}
