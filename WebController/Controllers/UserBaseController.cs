﻿using BaseModel.Enum.User.UserRole;
using DBModel_EFCore.User;
using MaYiWeb.Authention;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyInterface.Res;
using Server.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using ViewModel.User;
using ViewModel.Users.Baseinfo;
using Web.Authention;

namespace Web.Controllers
{
    public class UserBaseController:BaseController
    {
        private UserInfoServer Server { get; }
        public UserBaseController(UserInfoServer server) {
            Server = server;
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost("[Action]")]
        [AllowAnonymous]
        [ProducesResponseType(200,Type =typeof(BoolResult<LoginReturnModel>))]
        public IActionResult Login([FromBody]LoginModel login) {
            return Ok(Server.Login(login));
        }
        /// <summary>
        /// 登出用户
        /// </summary>
        /// <param name="model">登出自己时可以不填</param>
        /// <returns></returns>
        [RoleAuthorize( UserRoleEnum.普通用户,  UserRoleEnum.管理员)]
        [ProducesResponseType(200, Type = typeof(BoolResult))]
        [HttpPost("[Action]")]
        public IActionResult LogOut([FromBody] DefaultUseUserCodeView model = null)
        {
            return Ok(Server.LogOut(base.ServerContext));
        }
        ///// <summary>
        ///// 添加用户
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>
        //[AllowAnonymous]
        //[ProducesResponseType(200, Type = typeof(BoolResult))]
        //[HttpPost("[Action]")]
        //public IActionResult AddUser([FromBody] AddUserModel model)
        //{
        //    return Ok(Server.AddUser(model, false));
        //}
        /// <summary>
        /// 添加用户(管理员)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [RoleAuthorize( UserRoleEnum.管理员)]
        [ProducesResponseType(200, Type = typeof(BoolResult))]
        [HttpPost("[Action]")]
        public IActionResult AddUserAdmin([FromBody] AddUserModel model)
        {
            return Ok(Server.AddUser(model));
        }
        /// <summary>
        /// 激活用户
        /// </summary>
        /// <param name="model">激活自己时可以不填</param>
        /// <returns></returns>
        [RoleAuthorize( UserRoleEnum.管理员)]
        [ProducesResponseType(200, Type = typeof(BoolResult))]
        [HttpPost("[Action]")]
        public IActionResult EnableUser([FromBody] DefaultUseUserCodeView model)
        {
            return Ok(Server.EnableUser(base.ServerContext.UseUserID));
        }
        /// <summary>
        /// 锁定用户
        /// </summary>
        /// <param name="model">锁定自己的时候可以不填</param>
        /// <returns></returns>
        [RoleAuthorize( UserRoleEnum.管理员)]
        [ProducesResponseType(200, Type = typeof(BoolResult))]
        [HttpPost("[Action]")]
        public IActionResult DisableUser([FromBody] DefaultUseUserCodeView model = null)
        {
            return Ok(Server.DisableUser(base.ServerContext.UseUserID));
        }
        /// <summary>
        /// 用户信息
        /// </summary>
        /// <param name="model">查看自己信息的时候可以不填</param>
        /// <returns></returns>
        [RoleAuthorize( UserRoleEnum.普通用户,  UserRoleEnum.管理员)]
        [ProducesResponseType(200, Type = typeof(BoolResult<UserDetailsModel>))]
        [HttpPost("[Action]")]
        public IActionResult BaseInfo([FromBody] DefaultUseUserCodeView model)
        {
            return Ok(Server.UserDeatils(base.ServerContext.UseUserID));
        }
        /// <summary>
        /// 用户分页
        /// </summary>
        /// <returns></returns>
        [RoleAuthorize(UserRoleEnum.管理员)]
        [ProducesResponseType(200, Type = typeof(BoolResult<UserPageModel>))]
        [HttpPost("[Action]")]
        public IActionResult UserPage([FromBody] UserPageModel page)
        {
            return Ok(Server.UserPage(page));
        }
    }
}
