﻿using AllApplication;
using AllApplication.DBContext;
using AllApplication.Servers;
using Autofac;
using BaseModel.Enum.User.UserRole;
using DBModel_EFCore.User;
using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using MyInterface.Res;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using ViewModel;
using Web;
using Web.Handlers;

namespace WebApplication1.Options
{
    public class JwtHandler : JwtBearerHandler, IJwtToekn
    {
        public const string Audience = "api";
        public const string Issuer = "https://localhost";
        public const string UserCode = "uid";
        public const string UserTkn = "utn";
        public const string DesKey = "sdfsdfeertert";
        public const string JwtKey = "dfdfgeve453tedrfewerfvgergsf24e23";
        public static byte[] KeyBt => Encoding.UTF8.GetBytes(JwtKey);
        public const string AuthenToken = "User:Token";
        public const string AuthenKey = "Authorization";
        public const string JwtResult = "JwtResult";
        public const string RedisUserInfo = "User:UserInfo";
        public IRedisServer RedisServer { get; }
        private IDBContext DBContext { get; }
        public JwtHandler(IRedisServer redis, IDBContext dbcontext,
            IOptionsMonitor<JwtBearerOptions> options, ILoggerFactory logger, UrlEncoder url, IDataProtectionProvider dataprovd, ISystemClock clock
            ) : base(options, logger, url, dataprovd, clock)
        {
            //_container = container;
            RedisServer = redis;
            DBContext = dbcontext;
        }
      
        public async Task<AuthenticateResult> Jwtread() {
            Context.Items.Add(JwtOptions.UserTkn, null);
            Context.Items.Add(JwtOptions.UserCode, null);
            string authorization = Context.Request.Headers[AuthenKey];
            string token = "";
           
            AuthenticationFailedContext failedContext = new AuthenticationFailedContext(Context, Scheme, Options);
            if (string.IsNullOrEmpty(authorization))
            {
               // await Events.OnAuthenticationFailed(failedContext);
                return AuthenticateResult.Fail("数据异常");
            }
            if (authorization.StartsWith("Bearer"))
            {
             //   token = authorization;
                token = authorization.Substring("Bearer".Length).Trim();
            }
            if (string.IsNullOrEmpty(token))
            {
                await Events.OnAuthenticationFailed(failedContext);
                return AuthenticateResult.Fail("数据异常");
            }
            foreach (var validator in Options.SecurityTokenValidators)
            {
                if (validator.CanReadToken(token))
                {
                    SecurityToken validatedToken;
                    TokenValidatedContext tokenValidatedContext = new TokenValidatedContext(Context, Scheme, Options)
                    {
                    };
                    try
                    {
                        tokenValidatedContext.Principal = validator.ValidateToken(token, Options.TokenValidationParameters, out validatedToken);
                    }
                    catch (Exception e)
                    {
                        await Events.OnAuthenticationFailed(failedContext);
                        return tokenValidatedContext.Result;
                    }
                    if (validatedToken.Issuer != JwtOptions.Issuer)
                    {
                        await Events.OnAuthenticationFailed(failedContext);
                        return AuthenticateResult.Fail("颁发者不准确");
                    }
                    var usercode = tokenValidatedContext.Principal.Claims.FirstOrDefault(c => c.Type == JwtOptions.UserCode).Clone();
                    var usertoken = tokenValidatedContext.Principal.Claims.FirstOrDefault(c => c.Type == JwtOptions.UserTkn).Clone();
                     var redisinfo= await RedisServer.Database.StringGetAsync(RedisUserInfo + ":" + usercode);
                    UserInfo redisuserinfo = default;
                    if ( int.TryParse(usercode.Value,out var userid)&& !redisinfo.HasValue )
                    {
                        var userinfo = DBContext.Query<Users>().Where(g => g.ID == userid)
                          .Select(g => new
                          {
                              g.UserBase.Name,
                              g.ID,
                              roles = g.Roles.Select(r=>r.Role),
                              g.UserBase.Tel
                          }).FirstOrDefault();
                        if (userinfo == default)
                        {
                            await Events.OnAuthenticationFailed(failedContext);
                            return AuthenticateResult.Fail("用户未找到");
                        }
                        redisuserinfo = new UserInfo { ID = userinfo.ID, Name = userinfo.Name, Role = userinfo.roles.Select(g=>g+"") .ToList(), Tel= userinfo.Tel};
                        await RedisServer.Database.StringSetAsync(RedisUserInfo + ":" + usercode, JsonConvert.SerializeObject(redisuserinfo), when: StackExchange.Redis.When.NotExists);
                    }
                    else {
                        redisuserinfo = JsonConvert.DeserializeObject<UserInfo>(redisinfo.ToString());
                    }
                    var contextmodel= new ServerContextModel(tokenValidatedContext.Principal,Context)
                    {
                        UserID = Convert.ToInt32(usercode.Value),
                        DoPath = Context.Request.Path,
                        UserName = redisuserinfo.Name,
                        DoData = Context.ReadRequest<string>(),
                        UserRole= redisuserinfo.Role,
                        Tel=redisuserinfo.Tel
                    };
                    tokenValidatedContext.Principal = contextmodel;
                    contextmodel.RefreshRoleEvent += () =>
                    {
                        redisuserinfo.Role = contextmodel.UserRole;
                        RedisServer.Database.StringSetAsync(RedisUserInfo + ":" + usercode, JsonConvert.SerializeObject(redisuserinfo)).Wait();
                    };
                    if (contextmodel.IsInRole(UserRoleEnum.管理员.ToString())) {
                        string allflag = Context.Request.Headers["allshow"];
                        if (allflag != null) contextmodel.AllShowFlag = Convert.ToBoolean(allflag);
                        try
                        {
                            var jobj= JsonConvert.DeserializeObject<JObject>(contextmodel.DoData.ToString());
                            IUseUserCodeView useusercode = null;
                            if (jobj.TryGetValue(nameof(useusercode.UseUserCode), out var value)&& value.HasValues)
                            {
                                contextmodel.UseUserID = value.Value<int>();
                            }
                            else contextmodel.UseUserID = contextmodel.UserID;
                        }
                        catch { }
                    }
                    else contextmodel.UseUserID = Convert.ToInt32(usercode.Value);
                    
                    Context.User = contextmodel;
                    Context.Items[JwtOptions.UserTkn] = usertoken.Value;
                    Context.Items[JwtOptions.UserCode] = usercode.Value;
                    if (usercode == null || usertoken == null)
                    {
                        await Events.OnAuthenticationFailed(failedContext);
                        //return AuthenticateResult.Fail("数据缺失");
                    }
                    //var redtoken = RedisServer.Database.StringGetAsync(AuthenToken + ":" + Context.Items[JwtOptions.UserCode]).Result;
                    //if (!redtoken.HasValue || redtoken.ToString() != usertoken.Value)
                    //{
                    //    await Events.OnAuthenticationFailed(failedContext);
                    //    return AuthenticateResult.Fail("数据不正确或者已经过期，请重新登录");
                    //}
                    //RedisServer.Database.KeyExpireAsync(AuthenToken + ":" + Context.Items[JwtOptions.UserCode], TimeSpan.FromMinutes(30));

                    // 触发TokenValidated事件
                    await Events.OnTokenValidated(tokenValidatedContext);

                    // 验证成功
                    try
                    {
                        tokenValidatedContext.Success();
                    }
                    catch (Exception rgd)
                    {
                        var sdf = rgd.Message;
                    }
                    return tokenValidatedContext.Result;
                }
            }
            await Events.OnAuthenticationFailed(failedContext);
            return AuthenticateResult.Fail("未能解析数据");
        }
        /// <summary>
        /// 获取权限认证结果
        /// </summary>
        /// <returns></returns>
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            return await Jwtread();
        }
        /// <summary>
        /// 权限失败专项
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            Context.Request.Path = new PathString("/apiv2/Error/401");
           await  base.HandleChallengeAsync(properties);
            
        }
        /// <summary>
        /// 通过usercode获取token
        /// </summary>
        /// <param name="usercode"></param>
        /// <returns></returns>
        public string GetTokenString(int usercode)
        {
            var guid = Guid.NewGuid().ToString();
            var jwttoken = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new List<Claim> {
                      new Claim(JwtClaimTypes.Audience,Audience),
                      new Claim(JwtClaimTypes.Issuer,Issuer),
                      new Claim(UserCode,usercode+""),
                      new Claim(UserTkn,guid),
                 }),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(KeyBt), SecurityAlgorithms.HmacSha256Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenString = tokenHandler.WriteToken(tokenHandler.CreateToken(jwttoken));
            RedisServer.Database.StringSetAsync(AuthenToken + ":" + usercode, guid, TimeSpan.FromMinutes(30)).Wait();
            return tokenString;
        }
        /// <summary>
        /// 通过usercode 删除token
        /// </summary>
        /// <param name="usercode"></param>
        /// <returns></returns>
        public BoolResult DelToken(int usercode)
        {
            return RedisServer.Database.KeyDeleteAsync(AuthenToken + ":" + usercode).Result;
        }
        /// <summary>
        /// 通过usercode 刷新token
        /// </summary>
        /// <param name="usercode"></param>
        /// <returns></returns>
        public BoolResult RefreshToken(int usercode)
        {
            return RedisServer.Database.KeyExpireAsync(AuthenToken + ":" + usercode, TimeSpan.FromMinutes(30)).Result;
        }
        ///// <summary>
        ///// 获取权限认证filter时候的roles字符串
        ///// </summary>
        ///// <param name="roles"></param>
        ///// <returns></returns>
        //public static string GetAuthRole(params ModelEnum.User.UserRole.UserRoleEnum[] roles) => string.Join(",", roles.Select(g => g.ToString()).ToArray());
        private struct UserInfo {
            public string Name { get; set; }
            public int ID { get; set; }
            public List<string> Role { get; set; }
            public string Tel { get; set; }
        }
    }
}
