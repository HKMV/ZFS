﻿using AllApplication.Servers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web;

namespace MaYiWeb.Authention
{
    /// <summary>
    /// redis缓存mvc结果
    /// </summary>
    public class RedisResourseAttribute : Attribute, IResourceFilter
    {
        private IRedisServer RedisServer { get; }
        private const string Redis_Resource = "Resource";
        private bool returnflag;
        private string md5str;
        public RedisResourseAttribute(IRedisServer redis):this(redis,20)
        {
        }
        int second;
        public RedisResourseAttribute(IRedisServer redis, int second)
        {
            RedisServer = redis;
            this.second = second;
        }
        public void OnResourceExecuted(ResourceExecutedContext context)
        {
            if (returnflag) return;
            if (!(context.Result is ObjectResult jsonres)) return;
            if (jsonres.StatusCode != 200) return;
            RedisServer.Database.StringSetAsync(Redis_Resource + ":" + context.HttpContext.Request.Path + ":" + md5str,
                 JsonConvert.SerializeObject(jsonres.Value, new JsonSerializerSettings
                 {
                     ContractResolver = new CamelCasePropertyNamesContractResolver()//本来应该是获取mvcoptions的，找不到所以留一个坑
                 }),
                TimeSpan.FromSeconds(second == 0 ? 20 : second),
                StackExchange.Redis.When.NotExists);
        }

        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            var requestobject = context.HttpContext.ReadRequest<object>();
            if (requestobject == null) requestobject = new object();
            md5str = Commond.Commond.MD5str(JsonConvert.SerializeObject(requestobject));
            var resource = RedisServer.Database.StringGetAsync(Redis_Resource + ":" + context.HttpContext.Request.Path + ":" + md5str).Result;
            if (resource.HasValue)
            {
                var res = JsonConvert.DeserializeObject<Object>(resource.ToString());
                //res.Value
                context.Result = new JsonResult(res)
                {
                    StatusCode = 200,
                };

                returnflag = true;
            }
        }
    }

}
