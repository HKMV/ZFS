﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AllApplication;
using AllApplication.DBContext;
using AllApplication.Servers;
using Microsoft.AspNetCore.Http;

namespace Web.Handlers
{
    public class ServerContextModel : ClaimsPrincipal, IServerContext
    {
        public const string CheckRoleKey = "CheckRoleKey";
        protected HttpContext HttpContext { get; }
        public ServerContextModel(ClaimsPrincipal principal,HttpContext context) {
            base.AddIdentities(principal.Identities);
            HttpContext = context;
        }
        public string UserName { get; set; }
        public int UserID { get; set; }
        public List<string> UserRole { get; set; }
        public string DoPath { get; set; }
        public object DoData { get; set; }
        public string Tel { get; set; }

        public bool AllShowFlag { get; set; }
        public int UseUserID { get; set; }
        private List<string> checkedroles = new List<string>();
        public override bool IsInRole(string role)
        {
            if (!checkedroles.Contains(role)) checkedroles.Add(role);
            var flag= UserRole.Contains(role);
            HttpContext.Items[CheckRoleKey] = $"需求权限:{string.Join("或",role)},当前用户权限:{string.Join(",", UserRole)}";
            return flag;
        }
        /// <summary>
        /// 刷新redis的权限
        /// </summary>
        public void RefreshRole()
        {
            RefreshRoleEvent?.Invoke();
        }
        public event Action RefreshRoleEvent;

        //public bool IsInRole(string role) => IsInRole(role.ToString());
        public static string ClaimsCheckResult(HttpContext context) => context.Items[CheckRoleKey].ToString();

    }
}
