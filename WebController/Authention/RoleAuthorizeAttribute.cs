﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaseModel.Enum.User.UserRole;
using DBModel_EFCore.User;
using Microsoft.AspNetCore.Authorization;

namespace Web.Authention
{
    /// <summary>
    /// 方法需求的权限的确认
    /// </summary>
    public class RoleAuthorizeAttribute: AuthorizeAttribute
    {
        public  UserRoleEnum[] UserRoles { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleEnums"></param>
        public RoleAuthorizeAttribute(params  UserRoleEnum[] roleEnums) {
            UserRoles = roleEnums;
            Roles = string.Join(",", roleEnums.Select(g=>g+"").ToArray());
        }
    }
}
