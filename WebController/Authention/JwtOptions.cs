﻿//using IdentityModel;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
//using MyInterface.Res;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Options
{
    public class JwtOptions : IPostConfigureOptions<JwtBearerOptions>
    {
        public const string DesKey = "yaq-s.45";
        public const string JwtKey = "mayimayi3325895iuhu8oitrgmrksgoehjrfgieuswarhsdfearfew0124815";
        public const string Issuer = "https://api.myzuhao.top";
        public const string Audience = "api";
        public const string UserCode = "ucd";
        public const string UserTkn = "utn";
        public static byte[] KeyBt => Encoding.UTF8.GetBytes(JwtKey);
        ///// <summary>
        ///// 获取jwt的usercode
        ///// </summary>
        ///// <param name="usercode"></param>
        ///// <returns></returns>
        //public static string GetJwtUserCode(string usercode)
        //{
        //    return Commond.Commond.DESEncoded(usercode, DesKey);
        //}
        ///// <summary>
        ///// 获取db的usercode
        ///// </summary>
        ///// <param name="jwtcode"></param>
        ///// <returns></returns>
        //public static string GetDBUserCode(string jwtcode)
        //{
        //    return Commond.Commond.DESDecoded(jwtcode, DesKey); 
        //}
        public void PostConfigure(string name, JwtBearerOptions options)
        {
            options.Events = new JwtBearerEvents {
                OnMessageReceived = context => {
                    return Task.CompletedTask;
                },
#pragma warning disable CS1998 // 此异步方法缺少 "await" 运算符，将以同步方式运行。请考虑使用 "await" 运算符等待非阻止的 API 调用，或者使用 "await Task.Run(...)" 在后台线程上执行占用大量 CPU 的工作。
                OnAuthenticationFailed =async context => {
#pragma warning restore CS1998 // 此异步方法缺少 "await" 运算符，将以同步方式运行。请考虑使用 "await" 运算符等待非阻止的 API 调用，或者使用 "await Task.Run(...)" 在后台线程上执行占用大量 CPU 的工作。
                    //context.Response.Redirect("/api/user/jwt401");
                   

                }
            };
            options.TokenValidationParameters = new TokenValidationParameters
            {
                //NameClaimType = JwtClaimTypes.Name, //"lalala测试名字",
                //RoleClaimType = JwtClaimTypes.Role,// "lalala测试角色",
                ValidIssuer = Issuer,
                ValidAudience = Audience,
                IssuerSigningKey = new SymmetricSecurityKey(KeyBt),

            };
        }
    }
}
