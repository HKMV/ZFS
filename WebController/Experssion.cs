﻿using AllApplication.Servers;
using Autofac;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using MyInterface.Res;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.MiddleWares;
using WebApplication1.Options;

namespace Web
{
    public static class Experssion
    {
        /// <summary>
        /// 获取请求流的内容并重置
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="context"></param>
        /// <returns></returns>
        public static TResult ReadRequest<TResult>(this HttpContext context)
        {
            if (context.Request.ContentLength >= 16323) return default;
            if (context.Items.ContainsKey("readrequest")) return (TResult)context.Items["readrequest"];
            context.Request.EnableRewind();
            var stream = new StreamReader(context.Request.Body, Encoding.UTF8);
            context.Request.Body.Seek(0, SeekOrigin.Begin);
            string body = stream.ReadToEnd();
            context.Request.Body.Seek(0, SeekOrigin.Begin);
            try
            {
                context.Items["readrequest"] = body;
                return JsonConvert.DeserializeObject<TResult>(body);
            }
            catch
            {
                if (typeof(TResult) == typeof(string) || typeof(TResult) == typeof(object))
                    return (TResult)Convert.ChangeType(body, typeof(string));
                return default(TResult);
            }
        }
        /// <summary>
        /// 添加权限认证
        /// </summary>
        /// <param name="services"></param>
        public static void AddAuth(this IServiceCollection services)
        {
            var builder = services.AddAuthentication(option =>
            {
                option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                //option.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            });
            builder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<IPostConfigureOptions<JwtBearerOptions>, JwtOptions>());
            builder.AddScheme<JwtBearerOptions, JwtHandler>("Bearer", option => {
                ;
            });
            services.AddScoped<IJwtToekn, JwtHandler>();
            //     services.AddScoped<IMaYiContext,MaYiContextModel>();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static IActionResult OkBoolResult(this Controller controller, BoolResult result)
        {
            return controller.Ok(result);
        }
        /// <summary>
        /// 添加自定义中间件
        /// </summary>
        /// <param name="builder"></param>
        public static void AddMiddlerWare(this ContainerBuilder builder)
        {
            builder.RegisterType<HeaderMidder>().InstancePerLifetimeScope();
            builder.RegisterType<ExpersionMidder>().InstancePerLifetimeScope();
            //builder.RegisterType<LoggerMidder>().InstancePerLifetimeScope();
        }
        /// <summary>
        /// 使用自定义中间件
        /// </summary>
        /// <param name="builder"></param>
        public static void UseMiddleWare(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<HeaderMidder>();
            builder.UseMiddleware<ExpersionMidder>();
            //builder.UseMiddleware<LoggerMidder>();
        }
    }
}
