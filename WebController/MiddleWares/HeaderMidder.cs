﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Web.MiddleWares
{
    /// <summary>
    /// header中间件
    /// </summary>
    public class HeaderMidder : IMiddleware
    {
        /// <summary>
        /// 
        /// </summary>
        public HeaderMidder() {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {

            if (!context.Response.HasStarted)
            {
                context.Response.Headers["Access-Control-Allow-Methods"] = "GET,POST,PATCH,PUT,DELETE,OPTIONS";
                context.Response.Headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-Requested-With";
                if (context.Request.Method == HttpMethods.Post)
                {
                    string content = context.Request.Headers["Content-Type"].ToString().ToLower();
                    if (string.IsNullOrEmpty(content) || content == "text/plain")
                        context.Request.Headers["Content-Type"] = "application/json";
                }
                // context.Response.Headers["Content-Type"] = "application/json;charset=utf-8";
                //context.Response.OnStarting(async () =>
                //{
                //    if (string.IsNullOrEmpty(context.Response.Headers["Content-Type"].ToString())) 

               //context.Response.Headers["Content-Type"] = "application/json; charset=utf-8";
                //});
                await next.Invoke(context);
                //context.Features
                //context.Response.Headers["Content-Type"] = "appliction/json; charset=UTF-8";
            }
        }
    }
   
}
