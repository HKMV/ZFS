﻿using AllApplication.DBContext;
using Autofac;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Web.MiddleWares
{
    /// <summary>
    /// 日志
    /// </summary>
    public class LoggerMidder : IMiddleware
    {
        private ILogger<LoggerMidder> Nlogger { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nlog"></param>
        public LoggerMidder(ILogger<LoggerMidder> nlog) {
            Nlogger = nlog;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var startime = DateTime.Now;
            if (context.Request.Method == HttpMethods.Options) {
                await next(context);
                return;
            }
            bool readflag = false;
            string contecttype = context.Request.Headers["Content-Type"];
            if (!string.IsNullOrEmpty(contecttype)&&contecttype.Contains("json")) readflag = true;
   
            await next.Invoke(context);
            if (readflag)
            {
                string str = context.ReadRequest<object>()?.ToString();
                var stoptime = DateTime.Now.Subtract(startime);
                // Console.WriteLine($"url:{context.Request.Path},Method:{context.Request.Method},body:{str}");
                string url = context.Request.Path.ToString() + context.Request.QueryString;
                string ip = "未知";
                Task.Run(() =>
                {
                    using (var scope = Startup.Container.BeginLifetimeScope())
                    {
                        //var mayicontext = scope.Resolve<IDBContext>();
                        //mayicontext.Add(new Model.Infos.SysLog
                        //{
                        //    Contet = str,
                        //    Date = DateTime.Now,
                        //    Ip = ip,
                        //    Url = url,
                        //    SpendTime=new DateTime(stoptime.Ticks)
                        //});
                        //mayicontext.Commit();
                    }
                });

              //     context.Response.EnableResponseRead();
            }
        }
    }
}
