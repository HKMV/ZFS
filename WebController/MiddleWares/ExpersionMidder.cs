﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using MyInterface.Res;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.MiddleWares
{
    /// <summary>
    /// 错误捕捉
    /// </summary>
    public class ExpersionMidder : IMiddleware
    {
        private ILogger<ExpersionMidder> Nlogger { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public ExpersionMidder(ILogger<ExpersionMidder> logger) {
            Nlogger = logger;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            //Microsoft.AspNetCore.WebUtilities.MultipartReader.ReadHeadersAsync
            try
            {
           //     throw new Exception("sdfsdfsdfsdf");
                await next(context);
            }
            catch (Exception e) {
                Nlogger.LogError(e.ToString());
                try
                {
                    context.Response.StatusCode = 200;
                //    context.Response.Headers["Content-Type"] = "apppliction/json;charset:utf-8";
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(new BoolResult { Res = "The system is busy. Please try again later." }));
                }
                catch
                {

                }
            }
        }
    }
}
