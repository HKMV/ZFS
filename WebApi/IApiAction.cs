﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ApiClient
{
    public interface IApiAction<TSendModel,TReturnModel>
    {
        /// <summary>
        /// 请求的方法
        /// </summary>
        HttpMethod Method { get; }
        /// <summary>
        /// 请求的路径
        /// </summary>
        string Uri { get; }
        TSendModel SendValue { get; set; }
        TReturnModel ReturnValue { get; set; }
        bool SendAuth { get; }
        Dictionary<string, string> Query { get; }
        Dictionary<string, string> Header { get; }
        ApiClient ApiClient { get;set; }
    }
    public abstract class BaseiApiAction<TSendModelm, TReturnModel> : IApiAction<TSendModelm, TReturnModel>
    {
        public abstract HttpMethod Method { get; }
        public abstract string Uri { get; }
        public virtual TSendModelm SendValue { get; set; }
        public virtual TReturnModel ReturnValue { get; set; }
        public abstract bool SendAuth { get; }
        public virtual Dictionary<string, string> Query { get; }
        public virtual Dictionary<string, string> Header { get; }

        public ApiClient ApiClient { get; set; }
    }
}
