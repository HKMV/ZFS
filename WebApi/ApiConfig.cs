﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfContext.Commond;

namespace ApiClient
{
    public class ApiConfig:BaseSaveConfig<ApiConfig>
    {
        public ApiConfig(FileReader file) : base(file)
        {
        }
        public string BaseUrlstr { get; set; }
        [JsonIgnore]
        public Uri BaseUrl => new Uri(BaseUrlstr);
    }
}
