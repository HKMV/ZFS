﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ApiClient
{
    public class ApiClient
    {
        private static HttpClient HttpClient { get; } = new HttpClient();
        static ApiClient(){
            ServicePointManager.DefaultConnectionLimit = 100;
            ServicePointManager.ServerCertificateValidationCallback +=
                    (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                  => true;
            ServicePointManager.SecurityProtocol =SecurityProtocolType.Tls12;
        }
        public ApiConfig ApiConfig { get; }
        public ApiClient(ApiConfig config) {
            ApiConfig = config;
        }
        public async Task<TReturnModel> DoAction< TSendModel, TReturnModel>(IApiAction<TSendModel, TReturnModel> action)  {
            try
            {
                action.ApiClient = this;
                Uri url = new Uri(ApiConfig.BaseUrl, action.Uri);
                WebRequest request = WebRequest.CreateHttp(url);
                request.Method = action.Method.Method;
                if (action.SendAuth)
                {
                   // request.Headers.Add("Authorization", "Bearer" + LoginUser.Token);
                }
                request.ContentType = "application/json; charset=utf-8";
                switch (action.Method.Method)
                {
                    case "Get":

                        {
                            //url=new Uri(url.ToString()+"?"+)
                        }
                        break;
                    default:
                        {
                            if (typeof(TSendModel) != typeof(DBNull))
                            {
                                var stream = await request.GetRequestStreamAsync();
                                Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(action.SendValue)).ToList().ForEach(b => stream.WriteByte(b));
                            }
                        }
                        break;
                }
                string html = "";
                using (var reponse = await request.GetResponseAsync())
                using (var reponsestrem = reponse.GetResponseStream())
                {
                    List<byte> bts = new List<byte>();
                    int b = 0;
                    while ((b = reponsestrem.ReadByte()) != -1) bts.Add((byte)b);
                    html = Encoding.UTF8.GetString(bts.ToArray());
                }
                var returnvalue = JsonConvert.DeserializeObject<TReturnModel>(html);
                action.ReturnValue = returnvalue;
                return action.ReturnValue;
            }
            catch (Exception error) {
                throw error;
            }
        }
    }
}
