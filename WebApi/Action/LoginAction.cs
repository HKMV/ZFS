﻿using MyInterface.Res;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel.User;

namespace ApiClient.Action
{
    public class LoginAction : BaseiApiAction<LoginModel,BoolResult<LoginReturnModel>>
    {
        public override HttpMethod Method => HttpMethod.Post;

        public override string Uri => "/api/UserBase/Login";

        public override bool SendAuth => false;
    }
}
