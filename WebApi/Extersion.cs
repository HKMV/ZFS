﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
namespace ApiClient
{
    public static class Extersion
    {
        public static void AddApiClient(this ContainerBuilder builder) {
            builder.RegisterType<ApiClient>().InstancePerLifetimeScope();
            builder.RegisterType<ApiConfig>().SingleInstance();
            //foreach (var action in Assembly.GetExecutingAssembly().GetTypes().Where(g => typeof(IApiAction).IsAssignableFrom(g) && !g.IsAbstract && !g.IsInterface)) {
            //    builder.RegisterType(action);
            //}
                
        }
    }
}
