﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfContext.Commond;

namespace ZFS改.Config
{
    public class LoginConfig : BaseSaveConfig<LoginConfig>
    {
        public LoginConfig(FileReader file) : base(file)
        {
        }
        public string LoginUrl { get; set; }
        public string Name { get; set; }
        public string Pwd { get; set; }
        public bool SaveFlag { get; set; }
        protected override LoginConfig SaveValue => SaveFlag ? base.SaveValue : new LoginConfig(base.FileReader)
        {
            LoginUrl = LoginUrl,
            SaveFlag = SaveFlag
        };
    }
}
