﻿/*
   绑定视图：Login.xaml
   文件说明：用于控制用户登录/退出
*/
using ApiClient;
using ApiClient.Action;
using Autofac;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using ViewModel.User;
using ZFS改.Config;
//using ZFSBridge;

namespace ZFS改
{
    /// <summary>
    /// 登录逻辑处理
    /// </summary>
    public class LoginViewModel : ViewModelBase
    {
        public WpfContext.Wpfcontext Context { get; }
        private LoginConfig LoginConfig { get; }
        private ApiClient.ApiClient ApiClient { get; }
        public LoginViewModel(WpfContext.Wpfcontext context, LoginConfig config,ApiClient.ApiClient client) {
            Context = context;
            ApiClient = client;
            LoginConfig = config;
            name = config.Name;
            pwd = config.Pwd;
            saveflag = config.SaveFlag;
            base.RaisePropertyChanged(nameof(Name));
            base.RaisePropertyChanged(nameof(Pwd));
            base.RaisePropertyChanged(nameof(SaveFlag));
            IsCancel = true;
            SignCommand = new RelayCommand(Login);
        }
        private string url,name,pwd;
        public ObservableCollection<string> Urls { get; set; } = new ObservableCollection<string>
        {
            "https://127.0.0.1:5000"
        };
        public string Url { get => url; set {
                url = value;
                LoginConfig.LoginUrl = value;
                ApiClient.ApiConfig.BaseUrlstr = value;
                base.RaisePropertyChanged();
            }
        }
        public string Name { get => name; set {
                name = value;
                LoginConfig.Name = value;
                base.RaisePropertyChanged();
            }
        }
        public string Pwd { get => pwd; set {
                pwd = value;
                LoginConfig.Pwd = value;
                base.RaisePropertyChanged();
            } }
        private bool saveflag;
        public bool SaveFlag { get => saveflag;set {
                saveflag = value;
                LoginConfig.SaveFlag = value;
                base.RaisePropertyChanged();
            } }
        private string processstr;
        public string ProcessStr { get=>processstr; set {
                processstr = value;
                base.RaisePropertyChanged();
            } }
        private bool isCancel;
        public bool IsCancel { get => isCancel;set {
                isCancel = value;
                base.RaisePropertyChanged();
            } }
        public RelayCommand SignCommand { get; }
        void Login() {
            IsCancel = false;
            try
            {
                ProcessStr = "登录中";
                var action = new LoginAction {
                    SendValue = new LoginModel {
                        Tel = Name,
                        Pwd =Pwd
                    }
                };
                var restask = Task.Run(() => ApiClient.DoAction(action).Result);
                restask.Wait(10 * 1000);
                if (!restask.IsCompleted) {
                    ProcessStr = "连接超时";
                    return;
                }
                if (!restask.Result) {
                    ProcessStr = restask.Result;
                    return;
                }
                Context.LoginUser = restask.Result;
                Context.ServerProvider.Resolve<MainWindow>().Show();
            }
            catch (Exception error)
            {

            }
            finally {
                IsCancel=true;
            }
        }
    }
}
