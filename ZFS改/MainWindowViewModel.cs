﻿using Autofac;
using BaseModel.Enum.User.UserRole;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfContext;

namespace ZFS改
{
   public  class MainWindowViewModel:ViewModelBase
    {
        public Wpfcontext Context { get; set; }

        private RelayCommand closeCommand;
        public RelayCommand CloseCommand { get {
                if (closeCommand == null)
                    closeCommand = new RelayCommand(() =>
                    {

                    });
                return closeCommand;
            } set {
                closeCommand = value;
                base.RaisePropertyChanged();
            }
        }
        private ObservableCollection<PageViewModel> pages;
        public ObservableCollection<PageViewModel> Pages { get => pages;set {
                pages = value;
                base.RaisePropertyChanged();
            } }
        private PageViewModel selectPage;
        public PageViewModel SelectPage { get => selectPage;set {
                selectPage = value;
                base.RaisePropertyChanged();
            } }
        private RelayCommand<RouteModel> exitCommand;
        public RelayCommand<RouteModel> ExitCommand
        {
            get
            {
                if (exitCommand == null)
                    exitCommand = new RelayCommand<RouteModel>(route =>
                    {
                        if (!route.ShowDialog)
                        {
                            if (Pages.FirstOrDefault(g => g.RouteModel.Path == g.RouteModel.Path) is PageViewModel page)
                            {
                                Pages.Remove(page);
                            }
                        }
                        else DialogHost.CloseDialogCommand.Execute(null, null);
                    });
                return exitCommand;
            }
            set
            {
                exitCommand = value;
                base.RaisePropertyChanged();
            }
        }
        private RelayCommand<RouteModel> openPageCommand;
        public RelayCommand<RouteModel> OpenPageCommand { get {
                if (openPageCommand == null)
                    openPageCommand = new RelayCommand<RouteModel>(route =>
                    {
                        OpenPage(new PageInvoker
                        {
                            RouteModel = route
                        });
                    });
                return openPageCommand;
            } set {
                openPageCommand = value;
                base.RaisePropertyChanged();
            }
        }
        /// <summary>
        /// 通过路由，执行任务。如果是弹窗则可以等待返回的值
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public async Task<object> OpenPage(PageInvoker page) {
            using (var scope = Context.ServerProvider.BeginLifetimeScope()) {
                var context = scope.Resolve<Wpfcontext>();
                context.Falture = Context.Falture;
                context.PageInvoker = page;
                if (!page.RouteModel.ShowDialog)
                {
                    if (Pages.FirstOrDefault(g => g.RouteModel.Path == page.RouteModel.Path) is PageViewModel pagemodel)
                    {
                    }
                    else
                    {
                        BaseUIDlg dlg = (BaseUIDlg)scope.Resolve(page.RouteModel.ViewDlgType);
                        dlg.UI.DataContext = dlg.Viewmodel;
                        pagemodel = new PageViewModel(page.RouteModel, dlg.UI);
                        Pages.Add(pagemodel);
                    }
                    SelectPage = pagemodel;
                    ((BaseUIViewModel)pagemodel.UI.DataContext).PageInvoker(page);
                    return null;
                }
                else {
                    var dlg = scope.Resolve(page.RouteModel.ViewDlgType) as BaseUIDlg;
                    dlg.UI.DataContext = dlg.Viewmodel;
                    return await DialogHostEx.ShowDialog(Context.ServerProvider.Resolve<MainWindow>(), dlg.UI);
                }
            }
        }
    }
   
}
