﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace ZFS改.Command
{
    public class PwdHelper: DependencyObject
    {
        public static readonly DependencyProperty PasswordProperty;
        static PwdHelper() {
            PasswordProperty = DependencyProperty.RegisterAttached("Password", typeof(string), typeof(PwdHelper), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    (dp, ex) =>
                    {
                        var box = dp as PasswordBox;
                        if (ex.NewValue+"" == box.Password) return;
                        box.Password =(string) ex.NewValue;
                    }
                ));
        }
        public PwdHelper() {
            var sdf = this;
        }
        public static string GetPassword(DependencyObject dp)
        {
            return (string)dp.GetValue(PasswordProperty);
        }

        public static void SetPassword(DependencyObject dp, string value)
        {
            dp.SetValue(PasswordProperty, value);
        }


        //public string Password { get => GetValue(PasswordProperty) as string; set => SetValue(PasswordProperty, value); }
    }
    public class PasswordBoxBehavior : Behavior<PasswordBox>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.PasswordChanged += OnPasswordChanged;
        }

        private static void OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox passwordBox = sender as PasswordBox;

            string password = PwdHelper.GetPassword(passwordBox);

            if (passwordBox != null && passwordBox.Password != password)
            {
                PwdHelper.SetPassword(passwordBox, passwordBox.Password);
            }
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.PasswordChanged -= OnPasswordChanged;
        }
    }
}
