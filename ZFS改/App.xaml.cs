﻿using ApiClient;
using Autofac;
using System.Windows;
using WpfContext;
using WpfContext.Commond;
using ZFS改.Config;

namespace ZFS改
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var container = ConfigServers();
            App.Current.Exit += (obj, ex) => container.Dispose();
            (MainWindow = container.Resolve<Login>()).Show();
            base.OnStartup(e);
        }
        /// <summary>
        /// 注册ico
        /// </summary>
        /// <returns></returns>
        private IContainer ConfigServers() {
            var builder = new ContainerBuilder();
            builder.Register<Wpfcontext>(cmd =>
            {
                Wpfcontext context = new Wpfcontext(cmd.Resolve<WpfConfig>())
                {
                    ServerProvider = cmd.Resolve<ILifetimeScope>(),
                };
                return context;
            }).InstancePerLifetimeScope();
            builder.RegisterType<WpfContext.WpfConfig>().SingleInstance();
            builder.RegisterType<FileReader>().SingleInstance();
            builder.Register<MainWindow>(cmd=> {
                return new MainWindow()
                {
                    DataContext = cmd.Resolve<MainWindowViewModel>()
                };
            }).SingleInstance();
            builder.RegisterType<MainWindowViewModel>().SingleInstance();
            builder.Register<Login>(cmd =>
            {
                var Login = new Login
                {
                    DataContext = cmd.Resolve<LoginViewModel>()
                };
                return Login;
            });
            builder.RegisterType<LoginViewModel>();
            builder.RegisterType<LoginConfig>();
            builder.AddApiClient();
       //     builder.RegisterType
            return builder.Build();
        }
    }
}
