﻿using AllApplication.DBContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyInterface.Res;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBModel_EFCore
{
    public abstract class IEntiy<Tmodel> : IEFCoreIAggregate where Tmodel :  IEntiy<Tmodel>, new()
    {
        [Key]
        public int ID { get; set; }
        /// <summary>
        /// 用来注册，返回的值随意，
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public virtual EntityTypeBuilder Regist(ModelBuilder builder)
        {
            return builder.Entity<Tmodel>();
            //if (this is ICode code)
            //{
            //    entry.HasIndex("Code");
            //}
        }
        /// <summary>
        /// 用来触发数据库创建后的事件
        /// </summary>
        /// <param name="context"></param>
        public virtual  void OnCreated(IDBContext context)
        {
        }
        /// <summary>
        /// 用来在数据库保存的时候检查数据
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public virtual BoolResult Check(EntityState state)
        {
            return true;
        }
    }
}
