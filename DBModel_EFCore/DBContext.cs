﻿using AllApplication.DBContext;
using AllApplication.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using MyInterface.Res;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBModel_EFCore
{
    public class MysqlContext : AllApplication.DBContext.DBContext
    {
        internal static List<IEFCoreIAggregate> Aggregates;
        static MysqlContext()
        {
            Aggregates = AppDomain.CurrentDomain.GetAssemblies().SelectMany(g => g.GetTypes())
                            .Where(g => typeof(IEFCoreIAggregate).IsAssignableFrom(g) && !g.IsAbstract && !g.IsInterface)
                            .Select(g => (IEFCoreIAggregate)Activator.CreateInstance(g)).ToList();
        }
        private Efcontext Efcontext { get; }
        public MysqlContext(DbContextOptions options, bool createflag)
        {
            Efcontext = new Efcontext(options, createflag, this);
            if (createflag && Efcontext.Database.EnsureCreated())
            {
                foreach (var agree in Aggregates)
                    agree.OnCreated(this);
                var chk = this.Commit();
                if (!chk) throw new Exception(chk);
            }
        }

        public override ITransaction BeginTransaction(System.Data.IsolationLevel level = System.Data.IsolationLevel.ReadCommitted)
        {
            return new EFTransaction(Efcontext, level);
        }

        protected override BoolResult AddAggeegate(IAggregate aggregate)
        {
            Efcontext.Set<IAggregate>().Add(aggregate);
            return true;
        }

        public override BoolResult Commit()
        {
            foreach (var entry in Efcontext.ChangeTracker.Entries().Where(g => g.State != EntityState.Detached && g.State != EntityState.Unchanged))
            {
                IEFCoreIAggregate aggregate = (IEFCoreIAggregate)entry.Entity;
                var chk = aggregate.Check(entry.State);
                if (!chk) return aggregate.GetType().Name + ":" + chk;
                switch (entry.State)
                {
                    case EntityState.Added:
                        {
                            if (entry.Entity is IUpdateTime time)
                            {
                                time.CreateTime = DateTime.Now;
                                time.CreateName = "系统";
                            }
                            break;
                        }
                    case EntityState.Modified:
                        {
                            if (entry.Entity is IUpdateTime time)
                            {
                                time.UpdateTime = DateTime.Now;
                                time.UpdateName = "系统";
                            }
                            break;
                        }
                }
            }
            Efcontext.SaveChanges();
            return true;
        }

        protected override IQueryable<TAggregate> QueryAgreegate<TAggregate>()
        {
            return Efcontext.Set<TAggregate>().AsQueryable();
        }

        protected override BoolResult RemoveAgreegate(IAggregate aggregate)
        {
            throw new NotImplementedException();
        }

        protected override BoolResult UpdateAgreegate(IAggregate aggregate)
        {
            throw new NotImplementedException();
        }
    }
    public class Efcontext : Microsoft.EntityFrameworkCore.DbContext
    {

        public Efcontext(DbContextOptions options, bool createflag, IDBContext context) : base(options)
        {

        }
        private static List<Type> agreetypes;
        private static readonly object registlock = new object();
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var agree in MysqlContext.Aggregates)
                agree.Regist(modelBuilder);
            //base.OnModelCreating(modelBuilder);
        }
    }
    public class EFTransaction : ITransaction
    {
        private Efcontext Efcontext;
        private IDbContextTransaction transaction;
        public EFTransaction(Efcontext efcontext, System.Data.IsolationLevel level)
        {
            Efcontext = efcontext;
            transaction = Efcontext.Database.BeginTransaction(level);
        }
        public void Commit()
        {
            transaction.Commit();
        }

        public void Dispose()
        {
            transaction.Dispose();
        }

        public void RollBack()
        {
            transaction.Rollback();
        }
    }
}
