﻿using AllApplication.DBContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyInterface.Res;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;

namespace DBModel_EFCore.User
{
    public class UserBase : IEntiy<UserBase>, IUpdateTime
    {
        [StringLength(13), Required]
        public string Tel { get; set; }
        [StringLength(30), Required]
        public string Pwd { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string HeadImage { get; set; }
        public string Email { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateName { get; set; }
        public DateTime UpdateTime { get; set; }
        public string UpdateName { get; set; }
        public override BoolResult Check(EntityState state)
        {
            if (!new Regex(@"^1[3-8]\d{9}$").IsMatch(Tel))
                return "请输入正确的手机号";
            return true;
        }
        public override EntityTypeBuilder Regist(ModelBuilder builder)
        {
            var entiy= base.Regist(builder);
             entiy.HasIndex(nameof(Tel));
            return entiy;
        }
    }
}
