﻿using System;
using System.Collections.Generic;
using System.Text;
using BaseModel.Enum.User.UserRole;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyInterface.Res;

namespace DBModel_EFCore.User
{
    public class UserRole:IEntiy<UserRole>
    {
        public  UserRoleEnum Role { get; set; }
        public override BoolResult Check(EntityState state)
        {
            if (!Enum.IsDefined(typeof(UserRoleEnum), Role)) return "设置的权限不存在";
            return true;
        }
    }
}
