﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using AllApplication.DBContext;
using BaseModel.Enum.User.UserRole;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyInterface.Res;

namespace DBModel_EFCore.User
{
    public class Users:IEntiy<Users>
    {
        public bool ActiveFlag { get; set; }
        public UserBase UserBase { get; set; }
        public UserCash UserCash { get; set; }
        public List<UserRole> Roles { get; set; } = new List<UserRole>();
        public override BoolResult Check(EntityState state)
        {
            if (state == EntityState.Added)
            {
                if (UserBase == null || UserCash == null) return "用户基础资料为实例化";
                if (Roles == null || Roles.Count == 0) return "用户权限未初始化";
            }
            return true;
        }
        public override EntityTypeBuilder Regist(ModelBuilder builder)
        {
            var sdf=base.Regist(builder);
            builder.Entity<Users>()
                 .HasMany(g => g.Roles)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);
            return sdf;
        }
        public override void OnCreated(IDBContext context)
        {

            if (context.Query<Users>().Count() >= 1) return;
            Users user = new Users
            {
                ActiveFlag = true,
                UserBase = new  UserBase
                {
                    Email = "lalala@qq.com",
                    HeadImage = "http://www.xugongping.com/wp-content/uploads/2016/10/19300001317260131176866103067_950.jpg",
                    Name = "1442241179",
                    Pwd = "123456789",
                    Tel = "13345678910",
                },
                UserCash = new UserCash
                {
                    AllMoney = 10000,
                },
            };
            var roles = context.Query<UserRole>().ToList();
            if (roles == null || roles.Count == 0) new UserRole().OnCreated(context);
            user.Roles.Add(new UserRole
            {
                 Role= UserRoleEnum.普通用户
            });
            user.Roles.Add(new UserRole
            {
                Role = UserRoleEnum.管理员
            });
            context.Add(user);
        }
    }
}
