﻿using AllApplication.DBContext;
using Microsoft.EntityFrameworkCore;
using MyInterface.Res;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel_EFCore.User
{
    public class UserCash : IEntiy<UserCash>, IUpdateTime,ITimeSpan
    {
        public DateTime CreateTime { get; set; }
        public string CreateName { get; set; }
        public DateTime UpdateTime { get; set; }
        public string UpdateName { get; set; }
        public byte[] Timestamp { get; set; }
        public decimal AllMoney { get; set; }
        public decimal SpanedMoney { get; set; }
        /// <summary>
        /// 全部冻结的资金
        /// </summary>
        public decimal AllFrozen { get; set; }
        public override BoolResult Check(EntityState state)
        {
            if (AllMoney < 0) return "账户金额不能小于0";
            if (SpanedMoney < 0) return "账户花费金额不能小于0";
            if (AllFrozen < 0) return "账户冻结金额不能为负数";
            return true;
        }
    }
}
