﻿using AllApplication.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyInterface.Res;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel_EFCore
{
    public interface IEFCoreIAggregate : IAggregate
    {
        BoolResult Check(EntityState state);
        EntityTypeBuilder Regist(ModelBuilder modelBuilder);
    }
}
