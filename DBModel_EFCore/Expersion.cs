﻿using AllApplication.DBContext;
using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MySqlConnector.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBModel_EFCore
{
    public static class Expersion
    {
        /// <summary>
        /// 使用mysql的数据库
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="Configuration"></param>
        /// <param name="addlog"></param>
        public static void AddMysqlDBContext(this ContainerBuilder builder, IConfiguration Configuration,bool addlog=false)
        {
            builder.Register<IDBContext>(
             cmd =>
             {
                 DbContextOptionsBuilder b = new DbContextOptionsBuilder();
                 var sdf = Configuration.AsEnumerable().First(f => f.Key == "ConnectionString:MysqlUser");
                 //    b.UseSqlServer("server=.;database=MaYi;trusted_connection=true");
                 // b.UseMySQL(sdf.Value);
                 var flag = Configuration.GetValue<bool>("ConnectionString:CreateFlag");
                 //Users user = new Model.User.User();//用来引用dll的
                 var  sqlbuild =b.UseMySql(sdf.Value, mysqloption =>
                 {

                 });
                 if(addlog)sqlbuild.UseLoggerFactory(new LoggerFactory(new[] { new Microsoft.Extensions.Logging.Console.ConsoleLoggerProvider((_,__)=> true, true) }));
                 MysqlContext context = new MysqlContext(b.Options, flag);
                 return context;
             }).InstancePerLifetimeScope();
        }
        /// <summary>
        /// 使用sqlite的数据库
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="Configuration"></param>
        /// <param name="addlog"></param>
        public static void AddSqLiteDBContext(this ContainerBuilder builder, IConfiguration Configuration, bool addlog = false)
        {
            builder.Register<IDBContext>(
             cmd =>
             {
                 DbContextOptionsBuilder b = new DbContextOptionsBuilder();
                 var sdf = Configuration.AsEnumerable().First(f => f.Key == "ConnectionString:SqlStr");
                 //    b.UseSqlServer("server=.;database=MaYi;trusted_connection=true");
                 // b.UseMySQL(sdf.Value);
                 var flag = Configuration.GetValue<bool>("ConnectionString:CreateFlag");
                 //Users user = new Model.User.User();//用来引用dll的
                 var sqlbuild = b.UseSqlite(sdf.Value, mysqloption =>
                 {

                 });
                 if (addlog) sqlbuild.UseLoggerFactory(new LoggerFactory(new[] { new Microsoft.Extensions.Logging.Console.ConsoleLoggerProvider((_, __) => true, true) }));
                 MysqlContext context = new MysqlContext(b.Options, flag);
                 return context;
             }).InstancePerLifetimeScope();
        }
    }
}
