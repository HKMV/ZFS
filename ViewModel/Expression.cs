﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ViewModel.DBExpression
{
    public static class DBExpression
    {
        /// <summary>
        /// 获取分页中的orders
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="page"></param>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, OrderByClass>> PageOrders<T>(this GroupPageModel<T> page)
        {
            List<KeyValuePair<string, OrderByClass>> orders = page.GetType().GetProperties().Where(g => typeof(OrderByClass).IsAssignableFrom(g.PropertyType))
                .Select(g => new KeyValuePair<string, OrderByClass>(g.Name, g.GetValue(page) as OrderByClass))
                .Where(g => g.Value != null && g.Value.Flag)
                .OrderByDescending(g => g.Value.Index)
                .ToList();
            return orders;

        }
        /// <summary>
        /// 自动分页
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="ViewT"></typeparam>
        /// <param name="ts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static IQueryable<T> PageTolist<T, ViewT>(this IQueryable<T> ts, GroupPageModel<ViewT> page)
        {
            if (page.PageIndex < 0) page.PageIndex = 0;
            return ts.Skip(page.PageIndex * page.ItemCount).Take(page.ItemCount);

        }
    }
}
