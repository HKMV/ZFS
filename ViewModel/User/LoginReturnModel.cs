﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel.User
{
    public class LoginModel
    {
        public string Tel { get; set; }
        public string Pwd { get; set; }
    }
    /// <summary>
    /// 登录返回的model
    /// </summary>
    public class LoginReturnModel
    {  /// <summary>
       /// token 在其他需要权限认证的接口中加入到header中 格式为 Authorization="Bearer"+Token
       /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 用户的基础数据
        /// </summary>
        public UserDetailsModel UserBase { get; set; }
    }
}
