﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel.User
{
   public class UserCashModel
    {
        public int ID { get; set; }
        public decimal AllMoney { get; set; }
        public decimal SpanedMoney { get; set; }
        public decimal AllFrozen { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}
