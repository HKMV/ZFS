﻿namespace ViewModel.Users.Baseinfo
{
    /// <summary>
    /// 添加模型
    /// </summary>
    public class AddUserModel
    {
        /// <summary>
        /// 添加的手机号
        /// </summary>
        public string Tel { get; set; }
        /// <summary>
        /// 添加的密码
        /// </summary>
        public string Pwd { get; set; }
    }
}
