﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ViewModel.User
{
    public class UserDetailsModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public string HeadImage { get; set; }
        public DateTime CreateTime { get; set; }
        public List<BaseModel.Enum.User.UserRole.UserRoleEnum> Roles { get; set; }
        public List<String> RolesStr => Roles.Select(g => g.ToString()).ToList();

        public decimal HashCash { get; set; }
    }
}
