﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel.User
{
    public class UserPageModel:GroupPageModel<UserPageItemModel>
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTimeComper CreateTimeComper { get; set; }
        public OrderByClass CreateTimeOrder { get; set; }

    }
    public class UserPageItemModel {
        public int ID { get; set; }
        public string Name { get; set; }
        public string HeadImage { get; set; }
        public DateTime CreateTime { get; set; }
        public bool ActiveFlag { get; set; }
    }
}
