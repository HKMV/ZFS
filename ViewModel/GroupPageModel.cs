﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel
{
    /// <summary>
    /// 分页model
    /// </summary>
    /// <typeparam name="TItem">item类型</typeparam>
    public abstract class GroupPageModel<TItem>
    {
        /// <summary>
        /// 当前的页数，从0开始
        /// </summary>
        public int PageIndex { get; set; } = 0;
        private int count = 20;
        /// <summary>
        /// 每页item的数量 默认20，最大不可超过50
        /// </summary>
        public int ItemCount
        {
            get => count; set
            {
                if (value > 50) value = 50;
                count = value;
            }
        }
        /// <summary>
        /// 所有页的数量
        /// </summary>
        public int PageCount => (int)Math.Ceiling((double)AllItemCount / (double)ItemCount);
        /// <summary>
        /// 所有item的数量
        /// </summary>
        public int AllItemCount { get; set; }
        /// <summary>
        /// 用来存放返回的数据的，不需要在请求分页的时候发送过来
        /// </summary>
        public List<TItem> Obj { get; set; } = new List<TItem>();
    }
    /// <summary>
    /// 时间类型的比较
    /// </summary>
    public class DateTimeComper 
    {
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; }
    }
    /// <summary>
    /// 排序类型
    /// </summary>
    public class OrderByClass
    {
        /// <summary>
        /// 是否生效次排序
        /// </summary>
        public bool Flag { get; set; }
        /// <summary>
        /// 倒序
        /// </summary>
        public bool Desc { get; set; }
        /// <summary>
        /// 排序的优先级别，越大越优先 默认0
        /// </summary>
        public int Index { get; set; }
    }
}
