﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel
{
    public interface IUseUserCodeView
    {
        /// <summary>
        /// 被使用的usercode,用于对context的useusercode赋值。如果非管理员则会忽略此属性，并且吧header里token的usercode强制赋值给context
        /// </summary>
        string UseUserCode { get; set; }
    }
    /// <summary>
    /// 默认的usercode
    /// </summary>
    public class DefaultUseUserCodeView : IUseUserCodeView
    {
        /// <summary>
        /// 被操作的usercode
        /// </summary>
        public string UseUserCode { get; set; }
    }
    /// <summary>
    /// 默认的usercode+int的id
    /// </summary>
    public class DefaultInt_UseUserCodeView : DefaultUseUserCodeView {
        /// <summary>
        /// 操作的id
        /// </summary>
        public int ID { get; set; }
    }
}
