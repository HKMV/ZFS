﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using AllApplication.Repository;
using MyInterface.Res;

namespace AllApplication.DBContext
{
    public interface IDBContext
    {
        BoolResult Add(IAggregate aggregate);
        BoolResult Remove(IAggregate aggregate);
        BoolResult Update(IAggregate aggregate);
        BoolResult Commit();
        IQueryable<TAggregate> Query<TAggregate>()where TAggregate:class,IAggregate;
        ITransaction BeginTransaction(System.Data.IsolationLevel level= System.Data.IsolationLevel.ReadCommitted);
    }
    public interface ITransaction :IDisposable{
        void Commit();
        void RollBack();
    }
}
