﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using AllApplication.Repository;
using MyInterface.Res;

namespace AllApplication.DBContext
{
    public abstract class DBContext: IDBContext
    {
        protected BoolResult DbContextCheck(Type  context) {
            if (typeof(DBContext).IsAssignableFrom(context)) return true;
            else return "类型错误";
        }
        public BoolResult Add(IAggregate aggregate)
        {
            return AddAggeegate(aggregate);
        }
        protected abstract BoolResult AddAggeegate(IAggregate aggregate);
        public IQueryable<TAggregate> Query<TAggregate>() where TAggregate :class, IAggregate
        {
            return QueryAgreegate<TAggregate>();
        }
        protected abstract IQueryable<TAggregate> QueryAgreegate<TAggregate>() where TAggregate: class, IAggregate;
        public BoolResult Remove(IAggregate aggregate)
        {
           return RemoveAgreegate(aggregate);
        }
        protected abstract BoolResult RemoveAgreegate(IAggregate aggregate);

        public BoolResult Update(IAggregate aggregate)
        {
             return UpdateAgreegate(aggregate);
        }
        protected abstract BoolResult UpdateAgreegate(IAggregate aggregate);
        public abstract BoolResult Commit();
        public abstract ITransaction BeginTransaction(System.Data.IsolationLevel level= System.Data.IsolationLevel.ReadCommitted);
        public TAgreegate CreateAgreegate<TAgreegate>() where TAgreegate:BaseAggregate<TAgreegate,DBContext>{
            return (TAgreegate)Activator.CreateInstance(typeof(TAgreegate), this);
        }
    }
}
