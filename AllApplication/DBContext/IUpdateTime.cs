﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AllApplication.DBContext
{
    public interface IUpdateTime
    {
        DateTime CreateTime { get; set; }
        [StringLength(10)]
        string CreateName { get; set; }
        DateTime UpdateTime { get; set; }
        [StringLength(10)]
        string UpdateName { get; set; }
    }
}
