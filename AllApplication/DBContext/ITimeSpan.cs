﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AllApplication.DBContext
{
    public interface ITimeSpan
    {
        [Timestamp]
        byte[] Timestamp { get; set; }
    }
}
