﻿using System;
using System.Collections.Generic;
using System.Text;
using StackExchange.Redis;

namespace AllApplication.Servers
{
    public interface IRedisServer
    {
        ConnectionMultiplexer _connection { get; }
        IDatabase Database { get; }
        StackExchange.Redis.IServer Server { get; }
        ISubscriber Subscriber { get; }
    }
}
