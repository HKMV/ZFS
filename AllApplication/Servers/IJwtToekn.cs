﻿using System;
using System.Collections.Generic;
using System.Text;
using MyInterface.Res;

namespace AllApplication.Servers
{
    public interface IJwtToekn
    {
        string GetTokenString(int  userid);
        BoolResult DelToken(int userid);
        BoolResult RefreshToken(int userid);
    }
}
