﻿using System;
using System.Collections.Generic;
using System.Text;
using AllApplication.DBContext;

namespace AllApplication.Servers
{
    public abstract class IDoMainServers
    {
        public IDBContext DBContext { get; protected set; }
        public IDoMainServers(IDBContext  dB) {
            DBContext = dB;
        }
    }
}
