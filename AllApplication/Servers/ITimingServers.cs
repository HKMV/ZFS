﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AllApplication.Servers
{
    public interface ITimingServers:IDisposable
    {
        /// <summary>
        /// 开始循环
        /// </summary>
        /// <returns></returns>
        Task StartLoop();
        /// <summary>
        /// 挂起循环
        /// </summary>
        /// <returns></returns>
        Task SuspendLoop();
        /// <summary>
        /// 继续循环
        /// </summary>
        /// <returns></returns>
        Task ContinueLoop();
    }
}
