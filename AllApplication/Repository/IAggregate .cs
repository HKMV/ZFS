﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using MyInterface.Res;
using AllApplication.DBContext;

namespace AllApplication.Repository
{
    public interface IAggregate
    {
        //void Regist(ModelBuilder builder);
        void OnCreated(IDBContext context);
    }
    /// <summary>
    /// 同常要创建一个聚合根，要用通过context来创建
    /// </summary>
    /// <typeparam name="Aggregate"></typeparam>
    /// <typeparam name="DBContext"></typeparam>
    public abstract class BaseAggregate<Aggregate,DBContext> : IAggregate
        where Aggregate:BaseAggregate<Aggregate,DBContext> 
        where DBContext:IDBContext
    {

        public abstract void OnCreated(IDBContext context);
        //public abstract void Regist(ModelBuilder builder);
    }
}
