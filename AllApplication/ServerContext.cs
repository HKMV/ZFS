﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AllApplication
{
    public interface IServerContext
    {
        /// <summary>
        /// 用户名字
        /// </summary>
        string UserName { get;  }
        /// <summary>
        /// 使用者的用户唯一码
        /// </summary>
        int UserID { get;  }
        /// <summary>
        /// 用户的权限
        /// </summary>
        List<string> UserRole { get;  }
        /// <summary>
        /// 用户的行为
        /// </summary>
        string DoPath { get;  }
        /// <summary>
        /// 做行为的数据
        /// </summary>
        object DoData { get;  }
        void RefreshRole();
        /// <summary>
        /// 是否操控全部（一般都是分页）
        /// </summary>
        bool AllShowFlag { get; set; }
        bool IsInRole(string role);
        /// <summary>
        /// 被使用的usercode
        /// </summary>
        int UseUserID { get; set; }
    }
    /// <summary>
    /// 权限范围
    /// </summary>
    public enum PowerEnum {
        管理员=1,自己,公开
    }
}
