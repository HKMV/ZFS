﻿using AllApplication.DBContext;
using AllApplication.Servers;
using MyInterface.Res;
using Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ViewModel.User;
using ViewModel.DBExpression;
using DBModel_EFCore.User;
using Microsoft.EntityFrameworkCore;
using AllApplication;
using ViewModel.Users.Baseinfo;
using Commond;
using BaseModel.Enum.User.UserRole;

namespace Server.User
{
    public class UserInfoServer : IDoMainServers
    {
        private IJwtToekn Jwt { get; }
        public UserInfoServer(IDBContext dB,IJwtToekn jwt) : base(dB)
        {
            Jwt = jwt;
        }
        public BoolResult<UserPageModel> UserPage(UserPageModel page) {
            Expression<Func<DBModel_EFCore.User.Users,bool>> lamba = g => true;
            if (page.ID != 0) lamba = lamba.And(g => g.ID == page.ID);
            if (!string.IsNullOrEmpty(page.Name)) lamba = lamba.And(g => g.UserBase.Name == page.Name);
            if (page.CreateTimeComper != null)
                lamba = lamba.And(g => g.UserBase.CreateTime >= page.CreateTimeComper.StartTime && g.UserBase.CreateTime <= page.CreateTimeComper.EndTime);
            var query = DBContext.Query<DBModel_EFCore.User.Users>().Where(lamba);
            page.Obj= query.PageTolist(page).Select(g=>new {
                g.UserBase.CreateTime,
                g.UserBase.HeadImage,
                g.ID,
                g.UserBase.Name,
                g.ActiveFlag
            }).ToList().Select(g=>new UserPageItemModel {
                 CreateTime=g.CreateTime,
                 HeadImage=g.HeadImage,
                 ID=g.ID,
                 Name=g.Name,
                 ActiveFlag=g.ActiveFlag
            }).ToList();
            return page;
        }
        public BoolResult<UserDetailsModel> UserDeatils(int ID) {
            var dbuser = DBContext.Query<DBModel_EFCore.User.Users>()
                .Where(g => g.ID == ID)
                .Select(g => new
                {
                    g.ID,
                    g.UserBase.Name,
                    g.UserBase.HeadImage,
                    g.UserBase.Tel,
                    g.UserBase.CreateTime,
                    g.UserBase.Email,
                }).FirstOrDefault();
            if (dbuser == null) return "未找到此id用户";
            return new UserDetailsModel
            {
                 CreateTime=dbuser.CreateTime,
                 Email=dbuser.Email,
                 HeadImage=dbuser.HeadImage,
                 ID=dbuser.ID,
                 Name=dbuser.Name,
                 Tel=dbuser.Tel
            };
        }
        public BoolResult<UserCashModel> UserCash(int ID) {
            var dbuser = DBContext.Query<DBModel_EFCore.User.Users>()
              .Where(g => g.ID == ID)
              .Select(g => new
              {
                  g.ID,
                  g.UserCash.AllFrozen,
                  g.UserCash.AllMoney,
                  g.UserCash.SpanedMoney,
                  g.UserCash.UpdateTime
              }).FirstOrDefault();
            if (dbuser == null) return "未找到此id用户";
            return new UserCashModel
            {
                ID = dbuser.ID,
                AllFrozen = dbuser.AllFrozen,
                AllMoney = dbuser.AllMoney,
                UpdateTime = dbuser.UpdateTime,
                SpanedMoney = dbuser.SpanedMoney,
            };
        }
        public BoolResult UpdateUserDetails(UserDetailsModel model) {
            var dbuser = DBContext.Query<DBModel_EFCore.User.Users>()
                .Where(g => g.ID == model.ID)
                .Select(g => g.UserBase)
                .FirstOrDefault();
            if (dbuser == null) return "未找到当前用户";
            if (!string.IsNullOrEmpty(model.HeadImage)) dbuser.HeadImage = model.HeadImage;
            if (!string.IsNullOrEmpty(model.Name)) dbuser.Name = model.Name;
            if (!string.IsNullOrEmpty(model.Tel)) dbuser.Tel = model.Tel;
            if (!string.IsNullOrEmpty(model.Email)) dbuser.Email = model.Email;
            return DBContext.Commit();
        }
        public BoolResult UpdateUserCassh(UserCashModel model)
        {
            var dbuser = DBContext.Query<DBModel_EFCore.User.Users>()
                .Where(g => g.ID == model.ID)
                .Select(g => g.UserCash)
                .FirstOrDefault();
            if (dbuser == null) return "未找到当前用户";
            dbuser.AllFrozen = model.AllFrozen;
            dbuser.AllMoney = model.AllMoney;
            dbuser.SpanedMoney = model.SpanedMoney;
            return DBContext.Commit();
        }
        public BoolResult EnableUser(int ID) {
            var dbuser = DBContext.Query<DBModel_EFCore.User.Users>()
           .Where(g => g.ID == ID)
           .FirstOrDefault();
            if (dbuser == null) return "未找到当前用户";
            dbuser.ActiveFlag = true;
            return DBContext.Commit();
        }
        public BoolResult DisableUser(int ID)
        {
            var dbuser = DBContext.Query<DBModel_EFCore.User.Users>()
           .Where(g => g.ID == ID)
           .FirstOrDefault();
            if (dbuser == null) return "未找到当前用户";
            dbuser.ActiveFlag = false;
            return DBContext.Commit();
        }
        public BoolResult<LoginReturnModel> Login(LoginModel login)
        {
            var user = DBContext.Query<Users>()
                    .Include(g => g.UserBase)
                    .Include(g => g.UserCash)
                    .Include(g => g.Roles)
                    .FirstOrDefault(g => g.UserBase.Tel == login.Tel && g.UserBase.Pwd == login.Pwd);
            if (user == null) return "未找到用户或者密码错误";
            if (!user.ActiveFlag) return "此用户已经被封禁";

            var tokenString = Jwt.GetTokenString(user.ID);
            return new LoginReturnModel
            {
                Token = tokenString,
                UserBase = new UserDetailsModel
                {
                    ID = user.ID,
                    CreateTime = user.UserBase.CreateTime,
                    
                    HashCash = user.UserCash.AllMoney,
                    HeadImage = user.UserBase.HeadImage,
                    Name = user.UserBase.Name,
                    Tel = user.UserBase.Tel,
                    Email = user.UserBase.Email,
                    Roles = user.Roles.Select(g=>g.Role).ToList()
                }
            };
        }
        /// <summary>
        /// 登出
        /// </summary>
        /// <param name="usercode"></param>
        /// <returns></returns>
        public BoolResult LogOut(IServerContext context)
        {
            return Jwt.DelToken(context.UseUserID);
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="add"></param>
        /// <returns></returns>
        public BoolResult AddUser(AddUserModel add)
        {
          
            void addaction()
            {
                if (DBContext.Query<DBModel_EFCore.User.UserBase>().Any(baseinfo => baseinfo.Tel == add.Tel))
                {
                    throw new MsgError("当前手机号已经被注册");
                }
                Users users = new Users();
                users.UserBase = new UserBase()
                {
                    Tel = add.Tel,
                    Name = add.Tel,
                    Pwd = add.Pwd,
                    HeadImage =""
                };
                users.UserCash = new UserCash();
                users.ActiveFlag = true;
                users.Roles = new List<UserRole> {
                     new UserRole{
                           Role= UserRoleEnum.普通用户
                     }
                };
                DBContext.Add(users);
                var commit = DBContext.Commit();
                if (!commit) throw new MsgError(commit);
            }
            try
            {
                addaction();
                return true;
            }
            catch (MsgError error)
            {
                return error.Message;
            }
        }

    }
}
