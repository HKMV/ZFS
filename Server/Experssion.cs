﻿using AllApplication.Servers;
using Autofac;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace Server
{
    public static class Experssion
    {
        static List<Type> Types;
        static Experssion()
        {
            Types = Assembly.GetExecutingAssembly().GetTypes().ToList();
        }

        /// <summary>
        /// 添加所有domain服务
        /// </summary>
        /// <param name="builder"></param>
        public static void AddDoMainServers(this ContainerBuilder builder)
        {
            foreach (var domainserver in Types.Where(g => typeof(IDoMainServers).IsAssignableFrom(g) && !g.IsAbstract && !g.IsInterface))
            {
                builder.RegisterType(domainserver).InstancePerLifetimeScope();
            }
        }
        public static void AddOtherServer(this ContainerBuilder builder, IConfiguration configuration)
        {
            builder.Register<RedisServer>(cmd => new RedisServer(b =>
            {
                b.EndPoints = configuration.GetValue<string>("Redis:Host").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(g => {
                    var point = new IPEndPoint(IPAddress.Parse(g.Split(':')[0]), Convert.ToInt32(g.Split(':')[1]));
                    return point;
                }).ToList();
                b.ConnectTimeout = configuration.GetValue<int>("Redis:ConnectTimeout");
                b.ResponseTimeout = configuration.GetValue<int>("Redis:ResponseTimeout");
                b.SyncTimeout = configuration.GetValue<int>("Redis:SyncTimeout");
                b.DataBase = configuration.GetValue<int>("Redis:DataBase");
            })).As<IRedisServer>().InstancePerLifetimeScope();
        }
    }
}
