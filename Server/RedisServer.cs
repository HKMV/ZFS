﻿using AllApplication.Servers;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace Server
{

    public class RedisServer : IRedisServer
    {
        private static Process redisprocess;
        static RedisServer() {
            //var sdf=  File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"/Redis-x64-3.2.100 (1)/启动.bat");
            redisprocess = new Process();
            redisprocess.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory+ "Redis-x64-3.2.100 (1)\\";
            redisprocess.StartInfo.FileName = redisprocess.StartInfo.WorkingDirectory + "启动.bat";
            redisprocess.Start();
            AppDomain.CurrentDomain.DomainUnload += (obj, ex) =>
            redisprocess?.Close();
        }

        public class RedisBuilder
        {
            public IEnumerable<EndPoint> EndPoints { get; set; }
            public int DataBase { get; set; }
            public int ConnectTimeout { get; set; } = 15000;
            public int SyncTimeout { get; set; } = 5000;
            public int ResponseTimeout { get; set; } = 15000;
        }
        public RedisBuilder RedisBuilderOptions { get; set; }
        //  private string _connectionstring;//=> ConfigurationManager.ConnectionStrings["Redis"].ConnectionString;
        public ConnectionMultiplexer _connection { get; }
        public IDatabase Database { get; }
        public IServer Server { get; }
        public ISubscriber Subscriber { get; }


        public RedisServer(Action<RedisBuilder> builder)
        {
            RedisBuilderOptions = new RedisBuilder();
            builder(RedisBuilderOptions);
            ConfigurationOptions options = new ConfigurationOptions();
            RedisBuilderOptions.EndPoints.ToList().ForEach(p => options.EndPoints.Add(p));
            options.ConnectTimeout = RedisBuilderOptions.ConnectTimeout;
            options.SyncTimeout = RedisBuilderOptions.SyncTimeout;
            options.ResponseTimeout = RedisBuilderOptions.ResponseTimeout;
            options.DefaultDatabase = RedisBuilderOptions.DataBase;
            _connection = ConnectionMultiplexer.Connect(options);
            Database = GetDatabase(options.DefaultDatabase.Value);
            Subscriber = _connection.GetSubscriber();
            Server = _connection.GetServer(RedisBuilderOptions.EndPoints.First());

        }
        public IDatabase GetDatabase(int n = -1)
        {
            return _connection.GetDatabase(n);
        }
        //public byte[] Get(string key)
        //{
        //    string s = Database.StringGet(key);
        //    return RedisBuilderOptions.Encoding.GetBytes(s);
        //}

        //public async Task<byte[]> GetAsync(string key, CancellationToken token = default)
        //{
        //    return await Task.Run<byte[]>(() =>
        //    {
        //        var t = Database.StringGetAsync(key);
        //        t.Wait(token);
        //        return RedisBuilderOptions.Encoding.GetBytes(t.Result);
        //    });
        //}

        //public void Refresh(string key)
        //{
        //    Database.KeyExpire(key, RedisBuilderOptions.DefaultTimeSpan);
        //}

        //public async Task RefreshAsync(string key, CancellationToken token = default)
        //{
        //    await Task.Run(() =>
        //    {
        //        Database.KeyExpireAsync(key, RedisBuilderOptions.DefaultTimeSpan).Wait(token);
        //    });

        //}

        //public void Remove(string key)
        //{
        //    Database.KeyDelete(key);
        //}

        //public async Task RemoveAsync(string key, CancellationToken token = default)
        //{
        //    await Task.Run(() =>
        //    {
        //        Database.KeyDeleteAsync(key).Wait(token);
        //    });
        //}

        //public void Set(string key, byte[] value, DistributedCacheEntryOptions options)
        //{
        //    Database.StringSet(key, RedisBuilderOptions.Encoding.GetString(value), options.AbsoluteExpirationRelativeToNow);
        //}

        //public async Task SetAsync(string key, byte[] value, DistributedCacheEntryOptions options, CancellationToken token = default)
        //{
        //    await Task.Run(() => Database.StringSetAsync(key, RedisBuilderOptions.Encoding.GetString(value), options.AbsoluteExpirationRelativeToNow).Wait(token));
        //}

        public void Dispose()
        {
            _connection.Close();
            _connection.Dispose();

        }
    }
}
