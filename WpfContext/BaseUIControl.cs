﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ViewModel.User;

namespace WpfContext
{
   public   class BaseUIControl:UserControl
    {

        public Wpfcontext Context { get; internal set; }
        public BaseUIDlg BaseUIDlg { get; internal set; }
    }
}
