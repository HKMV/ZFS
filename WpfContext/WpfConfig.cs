﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfContext.Commond;

namespace WpfContext
{
    public class WpfConfig : BaseSaveConfig<WpfConfig>
    {
        public string Name { get;
            set; }
        public WpfConfig(FileReader file) : base(file)
        {
        }
    }
}
