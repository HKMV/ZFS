﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfContext
{
    /// <summary>
    /// page执行的model
    /// </summary>
    public class PageInvoker {
        /// <summary>
        /// 要进行对象的route
        /// </summary>
        public RouteModel RouteModel { get; set; }
        /// <summary>
        /// 要传递的参数，当此任务是从菜单栏进入时，obj为空
        /// </summary>
        public object Obj { get; set; }
    }
    /// <summary>
    /// page的viewmodel
    /// </summary>
    public class PageViewModel : ViewModelBase
    {
        public RouteModel RouteModel { get; }
        public BaseUIControl UI { get; }
        public PageViewModel(RouteModel route, BaseUIControl ui)
        {
            RouteModel = route;
            UI = ui;
        }
    }
}
