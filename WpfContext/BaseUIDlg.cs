﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ViewModel.User;

namespace WpfContext
{
    public interface BaseUIDlg {
        BaseUIControl UI { get; }
        BaseUIViewModel Viewmodel { get; }
        RouteModel InitRoute(LoginReturnModel model);
    }
    public abstract class BaseUIDlg<View, ViewModel>:BaseUIDlg
        where View : BaseUIControl
        where ViewModel : BaseUIViewModel
    {
        public BaseUIDlg(WpfContext.Wpfcontext wpfcontext) {
            Context = wpfcontext;
            UILazy = new Lazy<View>(() => {
                var view = Context.ServerProvider.Resolve<View>();
                view.Context = Context;
                return view;
            });
            ViewmodelLazy = new Lazy<ViewModel>(() => {
                var viewmodel = Context.ServerProvider.Resolve<ViewModel>();
                viewmodel.Context = Context;
                viewmodel.Dlg = this;
                return viewmodel;
            });
        }
        public WpfContext.Wpfcontext Context { get; internal set; }
        public Lazy<View> UILazy { get; }
        public BaseUIControl UI => UILazy.Value;
        public Lazy<ViewModel> ViewmodelLazy { get; }
        public BaseUIViewModel Viewmodel => ViewmodelLazy.Value;

        public RouteModel InitRoute(LoginReturnModel model) {
            var route = initRoute(model);
            if (route == null) return null;
            route.ViewDlgType = this.GetType();
            route.ViewModelType = typeof(ViewModel);
            route.ViewType = typeof(View);
            return route;
        }
        /// <summary>
        /// 根据不同的用户来生成不同的路径，用来初始化页面菜单，格式  路径1_路径2_路径3。 设置的路径如果结束那就不会显示之后的路径
        /// 返回null或者""则忽略
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected  internal abstract RouteModel initRoute(LoginReturnModel model);
    }
}
