﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BaseModel.Enum.User.UserRole;
using ViewModel.User;

namespace WpfContext
{
    public class Wpfcontext
    {
        public Wpfcontext(WpfConfig config) {
            WpfConfig = config;
        }
        public ILifetimeScope ServerProvider { get => GetFalture<ILifetimeScope>(); set => SetFalture(value); }
     //   public List<UserRoleEnum> UserRoles { get => GetFalture<List<UserRoleEnum>>(nameof(UserRoles)); set => SetFalture(value, nameof(UserRoles)); }
     /// <summary>
     /// 配置文件
     /// </summary>
        public WpfConfig WpfConfig { get => GetFalture<WpfConfig>(); private set => SetFalture(value); }
        public Dictionary<string, object> Falture { get; set; } = new Dictionary<string, object>();
        /// <summary>
        /// 全局唯一的登录用户
        /// </summary>
        public LoginReturnModel LoginUser { get => GetFalture<LoginReturnModel>(); set => SetFalture(value); }
        /// <summary>
        /// 执行路由页面的时候传递的参数
        /// </summary>
        public PageInvoker PageInvoker { get => GetFalture<PageInvoker>(); set => SetFalture(value); }
        private readonly object falturelock = new object();
        public bool SetFalture<Type>(Type obj, string type = "") {
            string key = typeof(Type).Name;
            if (type != "") key = type;
            if (Falture.ContainsKey(key)) {
                Falture[key] = obj;
                return true;
            }
            lock (falturelock) {
                if (Falture.ContainsKey(key)) return false;
                Falture.Add(key, obj);
                return true;
            }
        }
        public bool DelFalture<Type>(string type = "") {
            string key = typeof(Type).Name;
            if (type != "") key = type;
            if (!Falture.ContainsKey(type)) return false;
            lock (falturelock) {
                if (!Falture.ContainsKey(type)) return false;
                Falture.Remove(key);
                return true;
            }
        }
        public Type GetFalture<Type>(string type = "") {
            string key = typeof(Type).Name;
            if (type != "") key = type;
            if (!Falture.ContainsKey(key)) return default;
            return (Type)Falture[key];
        }

        public IMsg Msg{ get => GetFalture<IMsg>(); set => SetFalture(value); }
    }
}
