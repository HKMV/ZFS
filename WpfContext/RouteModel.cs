﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfContext
{ /// <summary>
  /// 左侧菜单
  /// </summary>
    public class RouteModel
    {
        public static readonly HashSet<RouteModel> Roles;
        static RouteModel()
        {
            Roles = new HashSet<RouteModel>(new Result.Expansion.Expansion.FuncComper<RouteModel>(
                    (x, y) => x.Path == y.Path,
                    x => x.Path.GetHashCode()
                ));
        }
        /// <summary>
        /// 通过path来查找route
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static RouteModel FindRouteByPath(string path) => Roles.FirstOrDefault(g => g.Path == path);
        /// <summary>
        /// 通过view的type来查找route
        /// </summary>
        /// <typeparam name="View"></typeparam>
        /// <returns></returns>
        public static RouteModel FindRouteByView<View>() where View : BaseUIControl => Roles.FirstOrDefault(g => g.ViewType == typeof(View));
        /// <summary>
        /// 通过view的type来查找route
        /// </summary>
        /// <typeparam name="View"></typeparam>
        /// <returns></returns>
        public static RouteModel FindRouteByView(BaseUIControl view) => Roles.FirstOrDefault(g => g.ViewType == view.GetType());
        /// <summary>
        /// 通过viewmodel来查找route
        /// </summary>
        /// <typeparam name="ViewModel"></typeparam>
        /// <returns></returns>
        public static RouteModel FindRouteByViewModel<ViewModel>() where ViewModel : BaseUIViewModel => Roles.FirstOrDefault(g => g.ViewModelType == typeof(ViewModel));
        /// <summary>
        /// 通过viewmodel来查找route
        /// </summary>
        /// <typeparam name="ViewModel"></typeparam>
        /// <returns></returns>
        public static RouteModel FindRouteByViewModel(BaseUIViewModel model) => Roles.FirstOrDefault(g => g.ViewModelType == model.GetType());
        /// <summary>
        /// 通过viewdlg来查找route
        /// </summary>
        /// <typeparam name="ViewDlg"></typeparam>
        /// <returns></returns>
        public static RouteModel FindRouteByViewDlg<ViewDlg>() where ViewDlg : BaseUIDlg => Roles.FirstOrDefault(g => g.ViewDlgType == typeof(ViewDlg));
        /// <summary>
        /// 通过viewdlg来查找route
        /// </summary>
        /// <typeparam name="ViewDlg"></typeparam>
        /// <returns></returns>
        public static RouteModel FindRouteByViewDlg(BaseUIDlg dlg)  => Roles.FirstOrDefault(g => g.ViewDlgType == dlg.GetType());

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path">路径</param>
        /// <param name="name">显示的名字</param>
        /// <param name="showdlg">是否为弹窗显示</param>
        /// <param name="visible">如果不是弹窗，是否在菜单上显示</param>
        public RouteModel(string path, string name,bool showdlg,bool visible)
        {
            Visibility = visible ? Visibility.Visible : Visibility.Collapsed;
               Path = path;
            Name = name;
            ShowDialog = showdlg;
            if (!addrolemodel(this)) throw new Exception("添加的权限路径重复");
        }
        public bool ShowDialog { get; set; }
        private bool autoadd;
        private bool addrolemodel(RouteModel model)
        {
            //先判断自己之前是否被自动添加过
            if (Roles.FirstOrDefault(g => g.Path == model.Path) is RouteModel findautoadd)
            {
                if (!findautoadd.autoadd) return false;
                findautoadd.autoadd = false;
                findautoadd.Visibility = model.Visibility;
                return true;
            }
            //没有的话再添加
            var flag = Roles.Add(model);
            if (!flag) return false;
            //在判断他的上一级是否存在和自动添加上一级4
            if (model.Level == 0) return true;//0是最顶级的
            var paths = model.Path.Split('_');
            string path = string.Join("_", paths.Take(paths.Length - 1).ToArray());
            if (Roles.Any(g => g.Path == path)) return true;//有了就不去添加了。
            var addmodel = new RouteModel(path, paths[paths.Length - 2],false, model.Visibility== Visibility.Visible)
            {
                autoadd = true
            };
            return true;
        }
        public Visibility Visibility { get; private set; }
        public string Path { get; private set; }
        public string Name { get; private set; }
        public int Level => Path.Count(c => c == '_');
        public IEnumerable<RouteModel> Items => Roles.Where(g => g.Level == Level + 1 && Path + "_" + g.Name == g.Path).ToList();
        public override string ToString() => Path;
        public Type ViewType { get; set; }
        public Type ViewModelType { get; set; }
        public Type ViewDlgType { get; set; }
    }
}
