﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WpfContext.Commond
{
    public class FileReader
    {
        public FileReader()
        {
            ;
        }
        private const string _charset = "utf-8";
        public string ReadText(string filepath, string charset = _charset) => ReadText(filepath, Encoding.GetEncoding(charset));
        public T ReadT<T>(string filepath, string charset = _charset) => ReadT<T>(filepath, Encoding.GetEncoding(charset));
        public void SaveText(string filepath, string contet, string charset = _charset) => SaveText(filepath, contet, Encoding.GetEncoding(charset));
        public void SaveT<T>(string filepath, T t, string charset = _charset) => SaveText(filepath, JsonConvert.SerializeObject(t, new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All
        }), Encoding.GetEncoding(charset));
        public string ReadText(string filepath, Encoding encoding)
        {
            FileInfo info = GetFileInfo(filepath);
            return File.ReadAllText(filepath, encoding);
        }
        public T ReadT<T>(string filepath, Encoding encoding)
        {
            FileInfo info = GetFileInfo(filepath);
            return JsonConvert.DeserializeObject<T>(File.ReadAllText(filepath), new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });
        }
        public void SaveText(string filepath, string contet, Encoding encoding)
        {
            FileInfo info = GetFileInfo(filepath);
            File.WriteAllText(filepath, contet, encoding);
        }
        private FileInfo GetFileInfo(string filepath)
        {
            FileInfo info = new FileInfo(filepath);
            if (!info.Directory.Exists) info.Directory.Create();
            if (!info.Exists) info.Create().Close();
            return info;
        }
    }
}
