﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WpfContext.Commond
{
    /// <summary>
    /// 一个会自动保存的抽象类，需要实现filereader的那个构造函数
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseSaveConfig<T>:IDisposable where T:BaseSaveConfig<T>
    {
        private delegate void  setValue(T left,T right);
        private static setValue SetValue;
        static BaseSaveConfig() {
            typeof(T).GetProperties().Where(g=>g.GetGetMethod()!=null&&g.GetSetMethod()!=null)
                .ToList().ForEach(g =>
                {
                    ParameterExpression paramterleft = Expression.Parameter(typeof(T));
                    ParameterExpression paramterright = Expression.Parameter(typeof(T));
                    Expression<Action<T,T>>lamba=Expression.Lambda<Action<T,T>>( Expression.Assign(Expression.Property(paramterleft, g), 
                        Expression.Property(paramterright, g)),paramterleft,paramterright);
                    SetValue += new setValue(lamba.Compile());
                });
            ;
        }
        private void InitValue(T value) => SetValue?.Invoke((T)this, value);
        /// <summary>
        /// 将配置文件中值赋值到此类
        /// </summary>
        public void InitValue() => InitValue(FileReader.ReadT<T>(SavePath));
        [JsonIgnore]
        protected FileReader FileReader { get; }
        [JsonIgnore]
        public virtual string SavePath => $"Config/{typeof(T).Name}.json";
        /// <summary>
        ///在第一次创建配置文件的时候触发
        /// </summary>
        protected virtual void CreateDefaultConfig() {}
        /// <summary>
        /// 保存时候抽取的保存对象，默认就是自己，可以用来做一些特殊的处理
        /// </summary>
        [JsonIgnore]
        protected virtual T SaveValue { get =>(T)this; }
        public void Save()
        {
            FileReader.SaveT(SavePath, SaveValue);
        }
        /// <summary>
        /// 自动调用保存
        /// </summary>
        public void Dispose() => Save();

        private  BaseSaveConfig() { }
        public BaseSaveConfig(FileReader file) {
            if (file == null) return;//null就是啥json之类的
            FileReader = file;
            try
            {
                var config = FileReader.ReadT<T>(SavePath);
                InitValue(config);
            }
            catch(Exception error) {
                CreateDefaultConfig();
                Save();
            }
        }
    }
}
