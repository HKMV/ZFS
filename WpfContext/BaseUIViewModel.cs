﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfContext
{
    public abstract class BaseUIViewModel:ViewModelBase
    {
        public WpfContext.Wpfcontext Context { get; internal set; }
        public BaseUIDlg Dlg { get; internal set; }
        /// <summary>
        /// 处理进行任务的接口。通常是其他窗口切换到这个窗口时传递的参数。
        /// </summary>
        /// <param name="invoker"></param>
        public abstract void PageInvoker(PageInvoker invoker);
    }
}
