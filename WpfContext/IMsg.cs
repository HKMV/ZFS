﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfContext
{
    public interface IMsg
    {
        Task Error(string errpr, bool Host = true);
        Task Warring(string warring, bool Host = true);
        Task Info(string info, bool Host = true);
        Task<bool> Question(string msg, bool Host = true);

    }
}
