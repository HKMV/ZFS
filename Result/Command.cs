﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Commond
{
    public static class Commond
    {
        public static TEnum ConvertToEnum<TEnum>(this int i)
        {
            try
            {
                return (TEnum)Enum.ToObject(typeof(TEnum), i);
            }
            catch
            {
                return default;
            }
        }
        ///   <summary>
        ///   给一个字符串进行MD5加密
        ///   </summary>
        ///   <param   name="strText">待加密字符串</param>
        ///   <returns>加密后的字符串</returns>
        public static string MD5Encrypt(string strText)
        {
            return MD5.Create().ComputeHash(System.Text.Encoding.GetEncoding("utf-8").GetBytes(strText))
                .Select(b => Convert.ToString(b >> 4 & 0xf, 16) + Convert.ToString(b & 0xf, 16))
                .Aggregate((a,b)=>a+b)
                .ToUpper();
        }

        public static string MD5str(string strText)
        {
            return MD5.Create().ComputeHash(System.Text.Encoding.GetEncoding("utf-8").GetBytes(strText))
                .Select(b => Convert.ToString(b, 16))
               .Aggregate((a, b) => a + b)
                .ToUpper();
        }

        /// <summary>
        /// des加密
        /// </summary>
        /// <param name="contect"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string DESEncoded(string contect, string key)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);
            byte[] keyIV = keyBytes;
            byte[] inputByteArray = Encoding.UTF8.GetBytes(contect);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, provider.CreateEncryptor(keyBytes, keyIV), CryptoStreamMode.Write);
            cStream.Write(inputByteArray, 0, inputByteArray.Length);
            cStream.FlushFinalBlock();
            return Convert.ToBase64String(mStream.ToArray());
        }
        /// <summary>
        /// DES解密
        /// </summary>
        /// <param name="contect"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string DESDecoded(string contect, string key)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);
            byte[] keyIV = keyBytes;
            byte[] inputByteArray = Convert.FromBase64String(contect);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, provider.CreateDecryptor(keyBytes, keyIV), CryptoStreamMode.Write);
            cStream.Write(inputByteArray, 0, inputByteArray.Length);
            cStream.FlushFinalBlock();
            return Encoding.UTF8.GetString(mStream.ToArray());
        }
    }
    public class Mcomper<T> : IEqualityComparer<T>
    {
        public Mcomper(Func<T, T, bool> eq, Func<T, int> hash)
        {
            this.eq = eq;
            this.hash = hash;
        }
        private readonly Func<T, T, bool> eq;
        private readonly Func<T, int> hash;
        public bool Equals(T x, T y)
        {
            return eq(x, y);
        }

        public int GetHashCode(T obj)
        {
            return hash(obj);
        }
    }
}
