﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Result.Expansion
{
    public static class Expansion
    {
        /// <summary>
        /// 阿里的现金
        /// </summary>
        /// <param name="money"></param>
        /// <returns></returns>
        public static string AliAcountMoney(this decimal money)
        {
            string monstr = money.Accurate() + "";
            if (!monstr.Contains('.')) return int.Parse(monstr) + "";
            return (decimal)Convert.ToInt32(money * 100) / 100 + "";
        }
        /// <summary>
        /// 小数点精确
        /// </summary>
        /// <param name="money"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        /// 
        public static decimal Accurate(this decimal money, int length = 2)
        {
            int mx = 10;
            while (length-- > 1) mx *= 10;
            return Convert.ToDecimal((double)Convert.ToInt32(money * mx) / mx);
        }
        public static string ViewDatetime(this DateTime date, string format = "HH小时mm分ss秒") => date.ToString(format);
        public static string ViewDatetime(this TimeSpan date, string format = "HH小时mm分ss秒") => new DateTime(date.Ticks).ToString(format);
        /// <summary>
        /// 截取长度替换字符
        /// </summary>
        /// <param name="s"></param>
        /// <param name="length"></param>
        /// <param name="replace"></param>
        /// <returns></returns>
        public static string StringSubLength(this string s, int length = 10, string replace = "...")
        {
            if (string.IsNullOrEmpty(s)) return s;
            if (s.Count() <= length) return s;
            return s.Substring(0, length) + replace;
        }
        public class FuncComper<T> : IEqualityComparer<T>
        {
            private Func<T, T, bool> funcequals;
            private Func<T, int> funcgethashcode;
            public FuncComper(Func<T, T, bool> euqals, Func<T, int> hashcode) {
                funcequals = euqals;
                funcgethashcode = hashcode;
            }
            public bool Equals(T x, T y)
            {
                return funcequals(x, y);
            }

            public int GetHashCode(T obj)
            {
                return funcgethashcode(obj);
            }
        }
    }
}
