﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static MyInterface.Res.BoolResult;

namespace MyInterface.Res
{
    public class BoolResult
    {
        public enum MsgEnum
        {
            TRACE, DEBUG, INFO, WARN, ERROR, FATAL
        }

        public MsgEnum MsgLevel { get; set; }
        public string MsgLevelDesc => MsgLevel.ToString();
        private bool flag;

        public bool Flag
        {
            get => flag;
            set
            {
                if (value) MsgLevel = MsgEnum.INFO;
                else MsgLevel = MsgEnum.ERROR;
                flag = value;
            }
        }

        public string Res { get; set; }
        
        public static implicit operator BoolResult(bool b) {
            return new BoolResult { Flag = b };
        }
        public static implicit operator bool(BoolResult b)=>b.Flag;
        public static implicit operator BoolResult(string Res) {
            return new BoolResult { Res = Res,Flag=false };
        }
        public static implicit operator string(BoolResult b)=>b.Res?.ToString()??(b.flag?"成功":"失败");
    }
    public class BoolResult<T>: BoolResult
    {
        public T Obj { get; set; }
        public static implicit operator bool(BoolResult<T> b) => b.Flag;
        public static implicit operator BoolResult<T>( string b) =>new BoolResult<T> { Flag=false, Res=b} ;
        public static implicit operator T(BoolResult<T> b) => b.Obj;
        public static implicit operator BoolResult<T>(T b) =>new BoolResult<T> {Flag=true, Obj=b };
  
    }
}
