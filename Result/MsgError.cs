﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commond
{
    /// <summary>
    /// 自定义错误
    /// </summary>
    public class MsgError:Exception
    {
        public MsgError(string msg) : base(msg) { }
    }
}
